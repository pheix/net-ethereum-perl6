use v6.d;
use Test;

use Net::Ethereum;

plan 2;

constant gasqty = 10000000;
constant recid  = 1982;
my $unlockpassw = @*ARGS[0] // 'node0';

my $etalondata  = 'My interests are in the field of real-time systems programming, development modeling software for embedded systems, test automation and software verification. I\'m a Raku (Perl6) ♥ enthusiast as well, work on module porting and contributing.';

my $obj = Net::Ethereum.new(
    :api_url('http://127.0.0.1:8501'),
    :show_progress(True),
    :unlockpwd($unlockpassw),
);

my %pingh = $obj.node_ping;

if %pingh<retcode> == -1 {
    skip-rest('ethereum node is down');
}
elsif !($obj.solidity.IO.e) {
    skip-rest('solidity is not installed');
}
else {
    my %tst_h;
    my Str $tst_s;

    subtest {
        plan 19;

        my %i_hash;
        my %s_hash;
        my @hashes;

        %tst_h = $obj.compile_and_deploy_contract(
            :contract_path($*HOME.path ~ '/git/pheix-research/smart-contracts/sol/BigBro.sol'),
            :compile_output_path('./t/solctarget'),
        );

        ok %tst_h, 'compile_and_deploy_contract returned Hash';

        ok %tst_h<blockHash> ~~ m:i/^ 0x<xdigit>+ $/, 'compile_and_deploy_contract blockHash=' ~ %tst_h<blockHash>;

        ok %tst_h<blockNumber> ~~ m:i/^ 0x<xdigit>+ $/, 'compile_and_deploy_contract blockNumber=' ~ %tst_h<blockNumber>;

        ok %tst_h<contractAddress> ~~ m:i/^ 0x<xdigit>+ $/, 'compile_and_deploy_contract contractAddress=' ~ %tst_h<contractAddress>;

        ok %tst_h<from> eq $obj.eth_accounts[0], 'compile_and_deploy_contract from=' ~ %tst_h<from>;

        ok %tst_h<transactionHash> ~~ m:i/^ 0x<xdigit>+ $/, 'compile_and_deploy_contract transactionHash=' ~ %tst_h<transactionHash>;

        ok %tst_h<status> == 1, 'compile_and_deploy_contract status=' ~ %tst_h<status>;

        ok !'./t/solctarget/BigBro.abi'.IO.e, 'compile_and_deploy_contract unlink *.abi';

        ok !'./t/solctarget/BigBro.bin'.IO.e, 'compile_and_deploy_contract unlink *.bin';

        $tst_s = $obj.retrieve_contract( %tst_h<transactionHash> );
        ok ( $tst_s eq %tst_h<contractAddress> ), 'retrieve_contract address=' ~ $tst_s;

        diag("\n" ~ %tst_h.gist ~ "\n");

        %i_hash = %s_hash =
            rowid => recid,
            ref   => 'pheix.org',
            ip    => '78.47.192.226',
            ua    => 'Netscape Navigator 4',
            res   => '1900*1200',
            pg    => 'hello-world.html',
            cntry => 'DE'
        ;

        %s_hash<rowid>:delete;

        @hashes.push(
            $obj.sendTransaction(
                :account($obj.eth_accounts[0]),
                :scid(%tst_h<contractAddress>),
                :fname('init'),
                :fparams(Hash.new),
                :gas(gasqty),
            )
        );
        ok @hashes.tail ~~ m:i/^ 0x<xdigit>+ $/, 'sendTransaction init hash=' ~ @hashes.tail;

        for 1..5 {
            @hashes.push(
                $obj.sendTransaction(
                    :account($obj.eth_accounts[0]),
                    :scid(%tst_h<contractAddress>),
                    :fname('removeById'),
                    :fparams(%(rowid => $_)),
                    :gas(gasqty),
                )
            );
            ok @hashes.tail ~~ m:i/^ 0x<xdigit>+ $/, 'sendTransaction removeById hash=' ~ @hashes.tail;
        };

        @hashes.push(
            $obj.sendTransaction(
                :account($obj.eth_accounts[0]),
                :scid(%tst_h<contractAddress>),
                :fname('insert'),
                :fparams(%i_hash),
                :gas(gasqty),
            )
        );
        ok @hashes.tail ~~ m:i/^ 0x<xdigit>+ $/, 'sendTransaction insert hash=' ~ @hashes.tail;

        diag('wait hashes array len: ' ~ @hashes.elems);

        $obj.wait_for_transaction(:hashes(@hashes));
        %tst_h = $obj.contract_method_call('getCount', Hash.new);
        ok %tst_h<count> == 1, 'contract_method_call getCount returns ' ~ %tst_h<count>;

        %tst_h = $obj.contract_method_call('getById', %(rowid => %i_hash<rowid>));
        is-deeply %tst_h, %s_hash, 'contract_method_call for getById done with rowid=' ~ %i_hash<rowid>;
    }, 'testing BigBro smart contract';

    subtest {
        plan 6;

        %tst_h = $obj.compile_and_deploy_contract(
            :contract_path($*HOME.path ~ '/git/pheix-research/smart-contracts/sol/StoreBytesArray.sol'),
            :compile_output_path('./t/solctarget'),
        );

        ok %tst_h, 'compile_and_deploy_contract returned Hash';

        $tst_s = $obj.retrieve_contract(%tst_h<transactionHash>);
        ok ( $tst_s eq %tst_h<contractAddress> ), 'retrieve_contract address=' ~ $tst_s;

        is(
            $obj.wait_for_transaction(
                :hashes(
                    [
                        $obj.sendTransaction(
                            :account($obj.eth_accounts[0]),
                            :scid(%tst_h<contractAddress>),
                            :fname('pushRecord'),
                            :fparams(%(data => Buf[uint8].new($etalondata.encode))),
                            :gas(gasqty),
                        )
                    ]
                )
            ).elems,
            1,
            'new record is pushed to StoreBytesArray'
        );

        my %h = $obj.contract_method_call('getRecord', %(index => 0));
        is %h<data>.decode, $etalondata, 'retrieve etalon bytes';

        is(
            $obj.wait_for_transaction(
                :hashes(
                    [
                        $obj.sendTransaction(
                            :account($obj.eth_accounts[0]),
                            :scid(%tst_h<contractAddress>),
                            :fname('popRecord'),
                            :fparams(Hash.new),
                            :gas(gasqty),
                        )
                    ]
                )
            ).elems,
            1,
            'pop record from StoreBytesArray'
        );

        %h = $obj.contract_method_call('getLength', Hash.new);
        is %h<length>, 0, 'pop is successfull: zero length';
    }, 'testing StoreBytesArray smart contract';
}

done-testing;
