use v6.d;
use Test;

constant test_plan_close      = 28;
constant test_plan_keep_alive = 24;
use Net::Ethereum;

plan 2;

my $unlockpassw   = @*ARGS[0] // 'node0';

my $marshal_test1 = '0x70381af8000000000000000000000000000000000000000000000000000000000000000200000000000000000000000000000000000000000000000000000000000000e0000000000000000000000000000000000000000000000000000000000000012000000000000000000000000000000000000000000000000000000000000001600000000000000000000000000000000000000000000000000000000000000220000000000000000000000000000000000000000000000000000000000000026000000000000000000000000000000000000000000000000000000000000002a0000000000000000000000000000000000000000000000000000000000000001168747470733a2f2f70686569782e6f7267000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000f3235352e3235352e3235352e313031000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000984d6f7a696c6c612f352e3020286950686f6e653b20435055206950686f6e65204f5320375f30206c696b65204d6163204f53205829204170706c655765624b69742f3533372e35312e3120284b48544d4c2c206c696b65204765636b6f292056657273696f6e2f372e30204d6f62696c652f313141343635205361666172692f393533372e35332042696e67507265766965772f312e306200000000000000000000000000000000000000000000000000000000000000000000000000000009313930302a313238300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e2f666565646261636b2e68746d6c00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000025553000000000000000000000000000000000000000000000000000000000000';

my $marshal_test2 = '0x045ba8d1000000000000000000000000000000000000000000000000000000000000037b00000000000000000000000000000000000000000000000000000000000000e00000000000000000000000000000000000000000000000000000000000000120000000000000000000000000000000000000000000000000000000000000016000000000000000000000000000000000000000000000000000000000000001e0000000000000000000000000000000000000000000000000000000000000022000000000000000000000000000000000000000000000000000000000000002600000000000000000000000000000000000000000000000000000000000000010687474703a2f2f79616e6465782e727500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e37302e3131382e3132302e313036000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004c4d6f7a696c6c612f352e3020285831313b205562756e74753b204c696e7578207838365f36343b2072763a36302e3029204765636b6f2f32303130303130312046697265666f782f36302e3000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009313230302a313630300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b2f696e6465782e68746d6c00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000024649000000000000000000000000000000000000000000000000000000000000';

my @test_hashes = [
    {
        ref   => 'goo.gl',
        ip    => '127.0.0.1',
        ua    => 'Mozilla',
        res   => '640*480',
        pg    => 'index.html',
        cntry => 'RU'
    },
    {
        ref   => 'twitter.com',
        ip    => '127.0.0.11',
        ua    => 'IE',
        res   => '1900*1280',
        pg    => 'index2.html',
        cntry => 'US'
    },
    {
        ref   => 'ya.ru',
        ip    => '127.0.0.12',
        ua    => 'Opera',
        res   => '1024*768',
        pg    => 'index3.html',
        cntry => 'BY'
    },
    {
        ref   => 'pheix.org',
        ip    => '127.0.0.111',
        ua    => 'Safari',
        res   => '100*200',
        pg    => 'index4.html',
        cntry => 'UA'
    },
    {
        ref   => 'foo.bar',
        ip    => '127.0.0.254',
        ua    => 'Netscape',
        res   => '1152*778',
        pg    => 'index5.html',
        cntry => 'RO'
    },
];

my %f_params1 =
    rowid => 2,
    ref   => 'https://pheix.org',
    ip    => '255.255.255.101',
    ua    => 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) ' ~
              'AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 ' ~
              'Mobile/11A465 Safari/9537.53 BingPreview/1.0b',
    res   => '1900*1280',
    pg    => '/feedback.html',
    cntry => 'US',
;

my %f_params2 =
    rowid => 891,
    ref   => 'http://yandex.ru',
    ip    => '70.118.120.106',
    ua    => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:60.0) ' ~
             'Gecko/20100101 Firefox/60.0',
    res   => '1200*1600',
    pg    => '/index.html',
    cntry => 'FI',
;

my Str  $contract = ( '0x' ~ '0' x 40 );
my Str  $transact = %*ENV<TXHASH> // ( '0x' ~ '0' x 64 );

#close connection subtest
subtest {
    plan test_plan_close;
    my $obj = Net::Ethereum.new(
        api_url => 'http://127.0.0.1:8501',
        show_progress => False,
        unlockpwd     => $unlockpassw,
    );
    my %h = $obj.node_ping;
    if ( %h<retcode> == -1 ) {
        skip-rest('ethereum node is down');
    }
    else {
        $obj.tx_wait_sec = 1;
        is $obj.personal_unlockAccount, True, 'account is unlocked';
        $obj.abi = './t/solcoutput/BigBro.abi'.IO.slurp.Str;

        my Int  $tsts  = 1;
        my      %params;
        my      %tst_h;
        my      @tst_a;
        my Str  $tst_s;
        my Int  $tst_i;
        my Bool $tst_b;
        my Real $tst_r;

        if $transact !~~ m:i/^ <[0x]>+ $/ {
            ok(
                ($transact ~~ m:i/^ 0x<xdigit>**64 $/ ),
                'valid transaction hash ' ~ $transact
            );
            %tst_h = $obj.eth_getTransactionReceipt( $transact );
            ok(
                %tst_h,
                'eth_getTransactionReceipt from ' ~
                ( %tst_h<from> || 'undefined' )   ~
                ' (status: ' ~ ( %tst_h<status> || '-1' ) ~
                ', used gas: ' ~ :16( %tst_h<gasUsed> || '0x0' ) ~ q{)},
            );
            if :16(%tst_h<status>) == 1 {
                $contract = $obj.retrieve_contract($transact);
                ok(
                    ($contract ~~ m:i/^ 0x<xdigit>**40 $/),
                    'valid smart contract addr ' ~ $contract
                );
            }
            else {
                skip 'tx ' ~ $transact  ~ ' not mined', 1;
            }
        } else {
            skip 'no valid hash for eth_getTransactionByHash is passed through input args', 3;
        }

        $tst_s = $obj.marshal('insert', %f_params1);
        is $tst_s.lc, $marshal_test1.lc, 'marshal for insert()';

        $tst_s = $obj.marshal('setById', %f_params2);
        is $tst_s.lc, $marshal_test2.lc, 'marshal for setById()';

        if $contract !~~ m:i/^ <[0x]>+ $/ {
            $tst_s = $obj.sendTransaction(
                # :account($obj.eth_accounts[0]),
                :scid($contract),
                :fname('insert'),
                :fparams(%f_params2),
                :gas(4700000),
            );
            ok(
                $tst_s ~~ m:i/^ 0x<xdigit>+ $/,
                'sendTransaction insert hash=' ~ $tst_s,
            );
            $obj.wait_for_transaction( :hashes(@($tst_s)));
        } else {
            skip 'no valid smart contract hash is defined', 1;
        }

        $tst_i = $obj.deploy_contract_estimate_gas(
            $obj.eth_accounts[0],
            '0x' ~ './t/solcoutput/BigBro.bin'.IO.slurp.Str, %(Nil)
        );
        ok $tst_i > 0, 'deploy_contract_estimate_gas gas=' ~ $tst_i;
        is(
            $obj.contract_id.defined, False,
            'attr $.contract_id is not defined',
        );

        $tst_s = $obj.deploy_contract(
            $obj.eth_accounts[0],
            '0x' ~ './t/solcoutput/BigBro.bin'.IO.slurp.Str, %(Nil),
            $tst_i
        );
        ok(
            $tst_s ~~ m:i/^ 0x<xdigit>+ $/,
            'deploy_contract transaction hash=' ~ $tst_s
        );
        $obj.wait_for_transaction(
            hashes   => @($tst_s),
            iters    => 15,
            contract => True,
        );

        is(
            $obj.contract_id.defined, True,
            'attr $.contract_id is defined: ' ~ $obj.contract_id,
        );

        $tst_i =
            $obj.contract_method_call_estimate_gas('insert', %f_params1);
        ok(
            $tst_i > 0,
            'contract_method_call_estimate_gas insert gas=' ~ $tst_i,
        );

        $tst_i =
            $obj.contract_method_call_estimate_gas('setById', %f_params2);
        ok(
            $tst_i > 0,
            'contract_method_call_estimate_gas setById gas=' ~ $tst_i,
        );

        $tst_i =
            $obj.contract_method_call_estimate_gas('init', %(Nil));
        ok(
            $tst_i > 0,
            'contract_method_call_estimate_gas init gas=' ~ $tst_i,
        );

        $tst_i = $obj.contract_method_call_estimate_gas(
            'removeById',
            %(rowid => 1)
        );
        ok(
            $tst_i > 0,
            'contract_method_call_estimate_gas removeById gas=' ~ $tst_i,
        );

        $tst_i = $obj.contract_method_call_estimate_gas('getCount', %(Nil));
        ok(
            $tst_i > 0,
            'contract_method_call_estimate_gas getCount gas=' ~ $tst_i,
        );

        $tst_i =
            $obj.contract_method_call_estimate_gas('getMaxId', %(Nil));
        ok(
            $tst_i > 0,
            'contract_method_call_estimate_gas getMaxId gas=' ~ $tst_i,
        );

        $tst_s = $obj.sendTransaction(
            :account($obj.eth_accounts[0]),
            :scid($obj.contract_id),
            :fname('init'),
            :fparams(%(Nil)),
            :gas(4700000),
        );
        ok(
            $tst_s ~~ m:i/^ 0x<xdigit>+ $/,
            'sendTransaction init hash=' ~ $tst_s,
        );
        $obj.wait_for_transaction(:hashes(@($tst_s)));

        for (@test_hashes).kv -> $i, %hash {
            my $rowid = $i + 1;
            %tst_h = $obj.contract_method_call(
                'getById',
                %(rowid => $rowid)
            );
            is-deeply(
                %tst_h,
                %hash,
                'contract_method_call for getById done ' ~
                'for rowid=' ~ $rowid,
            );
        };

        # signing transactions on Parity
        my $recnum = $obj.contract_method_call('getCount', %(Nil))<count>;
        my $maxid  = $obj.contract_method_call('getMaxId', %(Nil))<rowid>;

        %params =
            from => $obj.eth_accounts[0],
            to => $obj.contract_id,
            gas => 4700000,
            data => $obj.marshal('removeById', {rowid => $maxid - 1});

        %tst_h = $obj.eth_signTransaction(
            :from(%params<from>),
            :to(%params<to>),
            :gas(%params<gas>),
            :data(%params<data>)
        );

        ok %tst_h<raw>:exists, 'eth_signTransaction raw data for removeById() exists';
        ok (%tst_h<raw> ~~ m:i/^ 0x<xdigit>+ $/), 'eth_signTransaction for removeById() is hex str';

        $tst_s = $obj.eth_sendRawTransaction(:data(%tst_h<raw>));
        ok ($tst_s ~~ m:i/^ 0x<xdigit>+ $/), 'eth_sendRawTransaction for removeById() ' ~ $tst_s;

        $obj.wait_for_transaction(:hashes(@($tst_s)));

        is ($recnum - 1), $obj.contract_method_call('getCount', %(Nil))<count>, 'signed remove by id successful';

        if !($obj.keepalive) {
            is(
                $obj.finalize_request, False,
                'close connection on not keep-alive'
            );
        } else {
            skip 'close connection', 1;
        }
    }
}, 'close connection subtest';

# keep-alive connection subtest
subtest {
    plan test_plan_keep_alive;
    my $obj = Net::Ethereum.new(
        api_url => 'http://127.0.0.1:8501',
        show_progress => False,
        unlockpwd     => $unlockpassw,
        keepalive     => True,
    );
    my %h = $obj.node_ping;
    if ( %h<retcode> == -1 ) {
        skip-rest('ethereum node is down');
    }
    else {
        if !($obj.check_ua_keepalive) {
            skip-rest('user-agent not supporting keep-alive');
        } else {
            $obj.tx_wait_sec = 1;
            is $obj.personal_unlockAccount, True, 'account is unlocked';
            $obj.abi = './t/solcoutput/BigBro.abi'.IO.slurp.Str;

            my Int  $tsts  = 1;
            my      %params;
            my      %tst_h;
            my      @tst_a;
            my Str  $tst_s;
            my Int  $tst_i;
            my Bool $tst_b;
            my Real $tst_r;

            if $transact !~~ m:i/^ <[0x]>+ $/ {
                ok(
                    ($transact ~~ m:i/^ 0x<xdigit>**64 $/ ),
                    'valid transaction hash ' ~ $transact
                );
                %tst_h = $obj.eth_getTransactionReceipt($transact);
                ok(
                    %tst_h,
                    'eth_getTransactionReceipt from ' ~
                    ( %tst_h<from> || 'undefined' )   ~
                    ' (status: ' ~ ( %tst_h<status> || '-1' ) ~
                    ', used gas: ' ~ :16( %tst_h<gasUsed> || '0x0' ) ~ q{)},
                );
                if :16(%tst_h<status>) == 1 {
                    $contract = $obj.retrieve_contract($transact);
                    ok(
                        ($contract ~~ m:i/^ 0x<xdigit>**40 $/),
                        'valid smart contract addr ' ~ $contract
                    );
                }
                else {
                    skip 'tx ' ~ $transact  ~ ' not mined', 1;
                }
            } else {
                skip 'no valid hash for eth_getTransactionByHash is passed through input args', 3;
            }

            $tst_s = $obj.marshal( 'insert', %f_params1 );
            is $tst_s.lc, $marshal_test1.lc, 'marshal for insert()';

            $tst_s = $obj.marshal( 'setById', %f_params2 );
            is $tst_s.lc, $marshal_test2.lc, 'marshal for setById()';

            if $contract !~~ m:i/^ <[0x]>+ $/ {
                $tst_s = $obj.sendTransaction(
                    # :account($obj.eth_accounts[0]),
                    :scid($contract),
                    :fname('insert'),
                    :fparams(%f_params2),
                    :gas(4700000),
                );
                ok(
                    $tst_s ~~ m:i/^ 0x<xdigit>+ $/,
                    'sendTransaction insert hash=' ~ $tst_s,
                );
                $obj.wait_for_transaction(:hashes(@($tst_s)));
            } else {
                skip 'no valid smart contract hash is defined', 1;
            }

            $tst_i = $obj.deploy_contract_estimate_gas(
                $obj.eth_accounts[0],
                '0x' ~ './t/solcoutput/BigBro.bin'.IO.slurp.Str, %(Nil)
            );
            ok $tst_i > 0, 'deploy_contract_estimate_gas gas=' ~ $tst_i;
            is(
                $obj.contract_id.defined, False,
                'attr $.contract_id is not defined',
            );

            $tst_s = $obj.deploy_contract(
                $obj.eth_accounts[0],
                '0x' ~ './t/solcoutput/BigBro.bin'.IO.slurp.Str, %(Nil),
                $tst_i
            );
            ok(
                $tst_s ~~ m:i/^ 0x<xdigit>+ $/,
                'deploy_contract transaction hash=' ~ $tst_s
            );
            $obj.wait_for_transaction(
                hashes   => @($tst_s),
                iters    => 15,
                contract => True,
            );

            is(
                $obj.contract_id.defined, True,
                'attr $.contract_id is defined: ' ~ $obj.contract_id,
            );

            $tst_i =
                $obj.contract_method_call_estimate_gas('insert', %f_params1);
            ok(
                $tst_i > 0,
                'contract_method_call_estimate_gas insert gas=' ~ $tst_i,
            );

            $tst_i = $obj.contract_method_call_estimate_gas(
                'setById',
                %f_params2
            );
            ok(
                $tst_i > 0,
                'contract_method_call_estimate_gas setById gas=' ~ $tst_i,
            );

            $tst_i =
                $obj.contract_method_call_estimate_gas('init', %(Nil));
            ok(
                $tst_i > 0,
                'contract_method_call_estimate_gas init gas=' ~ $tst_i,
            );

            $tst_i = $obj.contract_method_call_estimate_gas(
                'removeById',
                %(rowid => 1)
            );
            ok(
                $tst_i > 0,
                'contract_method_call_estimate_gas removeById gas=' ~ $tst_i,
            );

            $tst_i =
                $obj.contract_method_call_estimate_gas('getCount', %(Nil));
            ok(
                $tst_i > 0,
                'contract_method_call_estimate_gas getCount gas=' ~ $tst_i,
            );

            $tst_i =
                $obj.contract_method_call_estimate_gas('getMaxId', %(Nil));
            ok(
                $tst_i > 0,
                'contract_method_call_estimate_gas getMaxId gas=' ~ $tst_i,
            );

            $tst_s = $obj.sendTransaction(
                :account($obj.eth_accounts[0]),
                :scid($obj.contract_id),
                :fname('init'),
                :fparams(%(Nil)),
                :gas(4700000),
            );
            ok(
                $tst_s ~~ m:i/^ 0x<xdigit>+ $/,
                'sendTransaction init hash=' ~ $tst_s,
            );
            $obj.wait_for_transaction(:hashes(@($tst_s)));

            for (@test_hashes).kv -> $i, %hash {
                my $rowid = $i + 1;
                %tst_h = $obj.contract_method_call(
                    'getById',
                    %(rowid => $rowid)
                );
                is-deeply(
                    %tst_h,
                    %hash,
                    'contract_method_call for getById done ' ~
                    'for rowid=' ~ $rowid,
                );
            };
            if $obj.keepalive {
                is $obj.finalize_request, True, 'close connection';
            } else {
                skip 'close connection', 1;
            }
        }
    }
}, 'keep-alive connection subtest';

done-testing;
