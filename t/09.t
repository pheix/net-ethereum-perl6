use v6.d;
use Test;
use Test::Mock;

plan 8;

use Net::Ethereum;
use HTTP::UserAgent;

class Request {
    has $!responses = {
        ok      => '{"result":1, "id":1}',
        fail    => '<html></html>',
        err     => '{"error":{"code": 3, "message": "execution reverted: Dai/insufficient-balance", "data": "0x08c3"}, "id":1}',
        status  => '{"result":0, "id":1}',
        inval   => '{"invalid": "response", "id":1}',
        noid    => '{"result":0}',
        invalid => '{"result":0, "id":100}',
    };

    has $.key = 'ok';

    method setup_response(Str $key!) {
        if $!responses{$key}:exists {
            $!key = $key
        }
        else {
            $!key = 'ok';
        }

        return self;
    }

    method code {
        return $!key eq 'status' ?? 500 !! 200;
    }

    method content {
        return $!responses{$!key};
    }

    method status-line {
        return 'Internal server error';
    }
}

subtest {
    plan 1;

    my $ua = mocked(
        HTTP::UserAgent,
        returning => {
            check_ua_keepalive => { False },
            request => Request.new
        });

    my $obj = Net::Ethereum.new(
        :api_url('http://localhost/api'),
        :debug(False),
        :ua($ua)
    );

    lives-ok { $obj.node_request({}) }, 'node_request done';
}, 'valid request';

subtest {
    plan 1;

    my $ua = mocked(
        HTTP::UserAgent,
        returning => {
            check_ua_keepalive => { False },
            request => Request.new.setup_response('inval')
        });

    my $obj = Net::Ethereum.new(
        :api_url('http://localhost/api'),
        :debug(False),
        :ua($ua)
    );

    throws-like { $obj.node_request({}) },
        Net::Ethereum::X::JSONAPI::InvalidResponse,
        'throws exception on invalid response';
}, 'invalid response';

subtest {
    plan 1;

    my $ua = mocked(
        HTTP::UserAgent,
        returning => {
            check_ua_keepalive => { False },
            request => Request.new.setup_response('fail')
        });

    my $obj = Net::Ethereum.new(
        :api_url('http://localhost/api'),
        :debug(False),
        :ua($ua)
    );

    throws-like { $obj.node_request({}) },
        Net::Ethereum::X::JSONAPI::ParseResponse,
        'throws exception on JSON parse failure';
}, 'JSON parse failure';

subtest {
    plan 1;

    my $ua = mocked(
        HTTP::UserAgent,
        returning => {
            check_ua_keepalive => { False },
            request => Request.new.setup_response('err')
        });

    my $obj = Net::Ethereum.new(
        :api_url('http://localhost/api'),
        :debug(False),
        :ua($ua)
    );

    throws-like { $obj.node_request({}) },
        Net::Ethereum::X::JSONAPI,
        'throws exception on error response';
}, 'error response';

subtest {
    plan 1;

    my $ua = mocked(
        HTTP::UserAgent,
        returning => {
            check_ua_keepalive => { False },
            request => Request.new.setup_response('status')
        });

    my $obj = Net::Ethereum.new(
        :api_url('http://localhost/api'),
        :debug(False),
        :ua($ua)
    );

    throws-like { $obj.node_request({}) },
        Net::Ethereum::X::JSONAPI::InvalidResponseStatus,
        'throws exception on invalid response status';
}, 'invalid status';

subtest {
    plan 1;

    my $ua = mocked(
        HTTP::UserAgent,
        returning => {
            check_ua_keepalive => { False },
            request => Request.new.setup_response('noid')
        });

    my $obj = Net::Ethereum.new(
        :api_url('http://localhost/api'),
        :debug(False),
        :ua($ua)
    );

    throws-like { $obj.node_request({}) },
        Net::Ethereum::X::JSONAPI::InvalidID,
        'throws exception on invalid session identifier';
}, 'no id in response';

subtest {
    plan 1;

    my $ua = mocked(
        HTTP::UserAgent,
        returning => {
            check_ua_keepalive => { False },
            request => Request.new.setup_response('invalid')
        });

    my $obj = Net::Ethereum.new(
        :api_url('http://localhost/api'),
        :debug(False),
        :ua($ua)
    );

    throws-like { $obj.node_request({}) },
        Net::Ethereum::X::JSONAPI::InvalidID,
        'throws exception on invalid session identifier';
}, 'invalid id in response';

subtest {
    plan 1;

    my $obj = Net::Ethereum.new;

    throws-like { $obj.node_request({}) },
        Net::Ethereum::X::JSONAPI::URL,
        'throws exception on undefined endpoint';
}, 'no endpoint is setup';

done-testing;
