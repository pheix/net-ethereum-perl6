use v6.d;
use Test;

use Net::Ethereum;

plan 2;

constant gasqty = 3000000;
my $unlockpassw = @*ARGS[0] // 'node0';

my $obj = Net::Ethereum.new(
    :api_url('http://127.0.0.1:8501'),
    :unlockpwd($unlockpassw),
    :tx_wait_sec(2),
);

if $obj {
    my %h = $obj.node_ping;

    if %h<retcode> > -1 {
        subtest {
            plan 5;

            for ^5 {
                my $pwd_hex  = $obj.string2hex('h4ck3r' ~ $_);
                my $password = $obj.web3_sha3($pwd_hex);

                my $account  = $obj.personal_newAccount(:password($password));

                ok $account ~~ m:i/^ 0x<xdigit>**40 $/, 'account ' ~ $account;
            }


        }, 'add accounts';

        subtest {
            plan 3;

            my @accounts_eth = $obj.eth_accounts;
            my @accounts_get;

            for ^@accounts_eth.elems {
                @accounts_get.push($obj.get_account(:index($_)));
            }

            ok @accounts_eth.elems > 2, sprintf("found %d accounts", @accounts_eth.elems);
            is-deeply @accounts_eth, @accounts_get, 'eth and get accounts';
            is-deeply @accounts_eth, $obj.accounts, 'eth and cached accounts';

        }, 'check existed accounts';
    }
    else {
        skip-rest('ethereum node is down');
    }
}
else {
    skip-rest('ethereum object is missed');
}

done-testing;
