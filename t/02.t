# Usage:   raku ./t/02.t <contract_transaction_hash>
# example: raku ./t/02.t 0xe30dedec437706a889ddd99a00fa687b437a1e4f1ee073af93a44b78bbbb737b

use v6.d;
use Test;

use Net::Ethereum;

constant test_plan = 53;
constant msg       = 'Net::Ethereum is here!';
constant pwd       = 'node0';

my $obj = Net::Ethereum.new(
    api_url       => 'http://127.0.0.1:8501',
    show_progress => True,
    unlockpwd     => pwd,
);

my %h = $obj.node_ping;
if ( %h<retcode> == -1 ) {
    plan :skip-all<ethereum node is down>;
}
else {
    plan 2;

    my      %params;
    my      %tst_h;
    my      @tst_a;
    my Str  $accnt;
    my Str  $tst_s;
    my Int  $tst_i;
    my Bool $tst_b;
    my Real $tst_r;
    my Int  $block;
    my Str  $tx = %*ENV<TXHASH> // ( '0x' ~ '0' x 64 );

    my Str $contract = ( '0x' ~ '0' x 40 );
    my Str $transact = @*ARGS[0] // $tx;

    subtest {
        plan test_plan;

        @tst_a = $obj.personal_listWallets;

        ok @tst_a.elems >= 2, sprintf("found %d wallets on non-Parity node", @tst_a.elems);

        ok(
            (@tst_a[0]<accounts>:exists) && (@tst_a[1]<accounts>:exists),
            'found accounts data'
        );

        my %accnt0 = (@tst_a[0]<accounts>)[0] if @tst_a[0]<accounts>:exists;
        my %accnt1 = (@tst_a[1]<accounts>)[0] if @tst_a[1]<accounts>:exists;

        is %accnt0<address>, $obj.eth_accounts[0], 'valid primary account';
        is %accnt1<address>, $obj.eth_accounts[1], 'valid secondary account';

        $accnt = %accnt0<address>;

        if @tst_a[0]<status> eq 'Unlocked' {
            ok(
                $obj.personal_lockAccount(:account(%accnt0<address>)),
                'lock unlocked primary account'
            );

            @tst_a = $obj.personal_listWallets;
        }
        else {
            skip 'primary account is locked - no need to lock', 1;
        }

        is @tst_a[0]<status>, 'Locked', 'primary account locked status';
        is @tst_a[1]<status>, 'Unlocked', 'secondary account unlocked status';

        $tst_b = $obj.personal_unlockAccount(:account(%accnt0<address>), :password(pwd), :duration(120));
        ok $tst_b, 'personal_unlockAccount on non-Parity node';

        @tst_a = $obj.personal_listWallets;

        is (@tst_a[0])<status>, 'Unlocked', 'primary account unlocked status';

        my Str $hmsg  = $obj.string2hex(msg);
        my Str $msign = $obj.personal_sign(:message($hmsg));

        ok $msign ~~ m:i/^ 0x<xdigit>+ $/, 'message is signed no.1';
        is $obj.personal_ecRecover(:message($hmsg), :signature($msign)), $accnt,
            'signed account is valid no.1';

        $hmsg  = $obj.string2hex(msg);
        $msign = $obj.personal_sign(:message($hmsg), :account($accnt), :password(pwd));

        ok $msign ~~ m:i/^ 0x<xdigit>+ $/, 'message is signed no.2';
        is $obj.personal_ecRecover(:message($hmsg), :signature($msign)), $accnt,
            'signed account is valid no.2';

        $block = $obj.eth_blockNumber;
        if $block > 0 {
            my Str $_hex = '0x' ~ $block.base(16).Str;
            $tst_i = $obj.eth_getBalance($accnt, $_hex);
            ok ( $tst_i >= 0 ), 'eth_getBalance at block ' ~ $_hex ~ ': ' ~ $tst_i;
        }
        else {
            skip 'eth_getBalance: no blocks in chain', 1;
        }

        $tst_i = $obj.eth_getBalance($accnt, 'latest');
        ok ( $tst_i >= 0 ), 'eth_getBalance latest: ' ~ $tst_i;

        $tst_i = $obj.eth_getBalance($accnt, 'earliest');
        ok ( $tst_i >= 0 ), 'eth_getBalance earliest: ' ~ $tst_i;

        $tst_i = $obj.eth_getBalance($accnt, 'pending');
        ok ( $tst_i >= 0 ), 'eth_getBalance pending: ' ~ $tst_i;

        $block = $obj.eth_blockNumber;
        if $block > 0 {
            my Str $_hex = '0x' ~ $block.base(16).Str;
            $tst_i = $obj.eth_getTransactionCount(:data($accnt), :tag($_hex));
            ok(
                ( $tst_i >= 0 ),
                'eth_getTransactionCount at block ' ~ $_hex ~ ': ' ~ $tst_i,
            );
        }
        else {
            skip 'eth_getBalance: no blocks in chain', 1;
        }

        $tst_i = $obj.eth_getTransactionCount(:data($accnt), :tag('latest'));
        ok ( $tst_i >= 0 ), 'eth_getTransactionCount latest: ' ~ $tst_i;

        $tst_i = $obj.eth_getTransactionCount(:data($accnt), :tag('earliest'));
        ok ( $tst_i >= 0 ), 'eth_getTransactionCount earliest: ' ~ $tst_i;

        $tst_i = $obj.eth_getTransactionCount(:data($accnt), :tag('pending'));
        ok ( $tst_i >= 0 ), 'eth_getTransactionCount pending: ' ~ $tst_i;

        # curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"eth_getTransactionByHash","params":["0x6c88ed755019615667b6aeb6fa29ec254ee7f4b75f21f0f4b5a5be919ecb1e52"],"id":1}' http://localhost:8600
        if $transact !~~ m:i/^ <[0x]>+ $/ {
            ok(
                ($transact ~~ m:i/^ 0x<xdigit>**64 $/ ),
                'valid transaction hash ' ~ $transact
            );
            %tst_h = $obj.eth_getTransactionByHash( $transact );
            ok(
                %tst_h,
                'eth_getTransactionByHash block no.' ~
                :16( %tst_h<blockNumber> || '0x0' )  ~
                ' (block hash: ' ~ ( %tst_h<blockHash> || 'Nil' ) ~ q{)},
            );
            %tst_h = $obj.eth_getTransactionReceipt( $transact );
            ok(
                %tst_h,
                'eth_getTransactionReceipt from ' ~
                ( %tst_h<from> || 'undefined' )   ~
                ' (status: ' ~ ( %tst_h<status> || '-1' ) ~
                ', used gas: ' ~ :16( %tst_h<gasUsed> || '0x0' ) ~ q{)},
            );
            if :16(%tst_h<status>) == 1 {
                $contract = $obj.retrieve_contract( $transact );
                ok(
                    ($contract ~~ m:i/^ 0x<xdigit>**40 $/ ),
                    'valid smart contract addr ' ~ $contract
                );
            }
            else {
                skip 'tx ' ~ $transact  ~ ' not mined', 1;
            }
        } else {
            skip 'no valid hash for eth_getTransactionByHash is passed through input args', 4;
        }

        # curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"eth_getBlockTransactionCountByHash","params":["0x49cddd11d530b9d09c637149eeb92e0da3a063964e125773c849722c3ce6ad1b"],"id":1}' http://localhost:8600

        $tst_i = $obj.eth_getBlockTransactionCountByHash( ( %tst_h<blockHash> || ( '0x' ~ '0' x 64 ) ).Str );
        ok ( $tst_i >= 0 ), 'eth_getBlockTransactionCountByHash: ' ~ $tst_i;

        $tst_i = $obj.eth_getBlockTransactionCountByHash( '0x49cddd11d530b9d09c637149eeb92e0da3a063964e125773c849722c3ce6ad10' );
        ok ( $tst_i >= 0 ), 'eth_getBlockTransactionCountByHash (invalid block hash): ' ~ $tst_i;

        $tst_r = $obj.wei2ether( 0 );
        is $tst_r, 0, 'wei2ether: 0 wei';

        $tst_r = $obj.wei2ether( 1 );
        is $tst_r, 0.000000000000000001, 'wei2ether: 1 wei';

        $tst_r = $obj.wei2ether( 2000 );
        is $tst_r, 0.000000000000002, 'wei2ether: 20000 wei';

        $tst_r = $obj.wei2ether( 883928732033678000000000 );
        is $tst_r, 883928.732033678, 'wei2ether: 883928732033678000000000 wei';

        # curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"eth_getCode","params":["0xb3abfa488058dba76206071b3600ae6e0dec3205","0x1e"],"id":1}' http://localhost:8600

        if $contract !~~ m:i/^ <[0x]>+ $/ {
            $block = $obj.eth_blockNumber;
            if $block > 0 {
                my Str $_hex = '0x' ~ $block.base(16).Str;
                # ($_hex ~ '=' ~ $tst_i).say;
                $tst_s = $obj.eth_getCode( $contract, $_hex );
                ok $tst_s, 'eth_getCode data (block ' ~ $_hex ~ '): ' ~ ( $tst_s.chars - 2 ) ~ ' bytes';
            } else {
                skip 'no blocks in chain', 1;
            }

            $tst_s = $obj.eth_getCode( $contract,'earliest' );
            ok $tst_s, 'eth_getCode data (earliest): ' ~ ( $tst_s.chars - 2 ) ~ ' bytes';

            $tst_s = $obj.eth_getCode( $contract,'latest' );
            ok $tst_s, 'eth_getCode data (latest): ' ~ ( $tst_s.chars - 2 ) ~ ' bytes';

            $tst_s = $obj.eth_getCode( $contract,'pending' );
            ok $tst_s, 'eth_getCode data (pending): ' ~ ( $tst_s.chars - 2 ) ~ ' bytes';

            # web3.sha3("getMaxId()");
            %params = to => $contract, data => '0xd477a712';
            $tst_s = $obj.eth_call( %params );

            my $max_id = ($tst_s ne '0x') ?? :16( $tst_s ) !! 0;
            my $id_to_remove = ( $max_id - 3 ) > 0 ?? ( $max_id - 3 ) !! $max_id;
            my $id_to_remove_data = sprintf("%064x", $id_to_remove);

            ok $tst_s ~~ m:i/ 0x<xdigit>+ /, 'eth_call on getMaxId(): ' ~ $max_id;

            # web3.sha3("getCount()");
            %params = to => $contract, data => '0xa87d942c';
            $tst_s = $obj.eth_call( %params );
            my $rowcnt = ($tst_s ne '0x') ?? :16( $tst_s ) !! 0;
            ok $tst_s ~~ m:i/ 0x<xdigit>+ /, 'eth_call on getCount(): ' ~ $rowcnt;

            # web3.sha3("getById(uint256)");
            %params = to => $contract, data => '0xf4f4d2370000000000000000000000000000000000000000000000000000000000000002';
            $tst_s = $obj.eth_call( %params );
            ok $tst_s ~~ m:i/ 0x<xdigit>+ /, 'eth_call on getById(): ' ~ ( $tst_s.chars - 2 ) ~ ' bytes';

            %params = to => $contract, data => '0xd477a712';
            $tst_i = $obj.eth_estimateGas( %params );
            ok $tst_i > 0 , 'eth_estimateGas for getMaxId(): ' ~ $tst_i;

            %params = to => $contract, data => '0xa87d942c';
            $tst_i = $obj.eth_estimateGas( %params );
            ok $tst_i > 0, 'eth_estimateGas for getCount(): ' ~ $tst_i;

            %params = to => $contract, data => '0xf4f4d2370000000000000000000000000000000000000000000000000000000000000002';
            $tst_i = $obj.eth_estimateGas( %params );
            ok $tst_i > 0, 'eth_estimateGas for getById(): ' ~ $tst_i;

            ok './t/solcoutput/BigBro.abi'.IO.e, 'contract abi found';

            $obj.abi = './t/solcoutput/BigBro.abi'.IO.slurp.Str;
            ok $obj.abi, 'abi loaded';

            %params = from => $accnt, to => $contract, gas => sprintf("0x%x", 4700000), data => $obj.marshal('init', {});

            $tst_s = $obj.eth_sendTransaction( %params );
            ok $tst_s, 'eth_sendTransaction init() hash: ' ~ $tst_s;

            %params = from => $accnt, to => $contract, gas => sprintf("0x%x", 4700000), data => '0x5ae9d5da' ~ $id_to_remove_data.Str;
            $tst_s = $obj.eth_sendTransaction( %params );
            ok $tst_s, 'eth_sendTransaction removeById() hash: ' ~ $tst_s;

            $tst_i = $obj.eth_blockNumber;
            ok ( $tst_i > 0 ), 'eth_blockNumber: ' ~ $tst_i;

            # signing transactions on Parity
            %params = from => $accnt, to => $contract, gas => 4700000, data => $obj.marshal('removeById', {rowid => 1982});

            %tst_h = $obj.eth_signTransaction(
                :from(%params<from>),
                :to(%params<to>),
                :gas(%params<gas>),
                :data(%params<data>)
            );

            ok %tst_h<raw>:exists, 'eth_signTransaction raw data for removeById() exists';
            ok (%tst_h<raw> ~~ m:i/^ 0x<xdigit>+ $/), 'eth_signTransaction for removeById() is hex str';

            $tst_s = $obj.eth_sendRawTransaction(:data(%tst_h<raw>));
            ok ($tst_s ~~ m:i/^ 0x<xdigit>+ $/), 'eth_sendRawTransaction for removeById() ' ~ $tst_s;

            %params =
                from => $accnt,
                to   => $contract,
                gas  => 4700000,
                data => $obj.marshal(
                    'insert',
                    {
                        rowid => 1982,
                        ref   => 'index.html',
                        ip    => '127.0.0.1',
                        ua    => 'FireFox',
                        res   => '1024*768',
                        pg    => 'feedback.html',
                        cntry => 'RU'
                    }
                );

            %tst_h = $obj.eth_signTransaction(
                :from(%params<from>),
                :to(%params<to>),
                :gas(%params<gas>),
                :data(%params<data>)
            );

            ok %tst_h<raw>:exists, 'eth_signTransaction raw data for insert() exists';
            ok (%tst_h<raw> ~~ m:i/^ 0x<xdigit>+ $/), 'eth_signTransaction for insert() is hex str';

            $tst_s = $obj.eth_sendRawTransaction(:data(%tst_h<raw>));
            ok ($tst_s ~~ m:i/^ 0x<xdigit>+ $/), 'eth_sendRawTransaction for insert() ' ~ $tst_s;
            ok $obj.personal_lockAccount(:address($obj.eth_accounts[0])), 'lock account finally';
        } else {
            skip 'no valid smart contract hash is defined', 22;
        }
    }, 'Generic RPC methods';

    subtest {
        plan 2;

        my $maxpriorityfeepergas = $obj.eth_maxPriorityFeePerGas;

        ok $maxpriorityfeepergas, sprintf("eth_maxPriorityFeePerGas returned %d", $maxpriorityfeepergas);

        if $contract !~~ m:i/^ <[0x]>+ $/ {
            my $transaction = {
                from => $accnt,
                to   => $contract,
                gas  => sprintf("0x%x", 4700000),
                data => $obj.marshal('init', {})
            };

            my $access_list = $obj.eth_createAccessList(:$transaction);

            ok $access_list ~~ Hash &&
               $access_list.keys &&
               ($access_list<accessList>:exists) &&
               ($access_list<gasUsed>:exists), sprintf("eth_createAccessList gas usage %d", $access_list<gasUsed>.UInt);
        }
        else {
            skip 'no valid smart contract hash is defined', 1;
        }
    }, 'Extended RPC methods'
}

done-testing;
