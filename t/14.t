use v6.d;
use Test;
use Test::Mock;

use Net::Ethereum;
use Net::Ethereum::Utils;

constant eth     = Net::Ethereum.new;
constant block_1 = { blobGasUsed => 0x20000, excessBlobGas => 0x0 };
constant block_2 = { blobGasUsed => 0x20000, excessBlobGas => 393216 * 100 };

constant factor      = 1;
constant denominator = 3338477;

constant etalons = [1, 1, 1, 2, 2, 2, 3, 3, 4, 5, 5, 6, 7, 9, 10, 12, 14, 16, 19, 22, 25, 30, 34, 40, 47, 54, 63, 73, 85, 99, 115, 133, 155, 180, 209, 243, 282, 328, 381, 442, 513, 596, 692, 804, 933, 1084, 1259, 1462, 1697, 1971, 2288, 2657, 3085, 3583, 4160, 4830, 5609, 6512, 7562, 8780, 10195, 11838, 13745, 15960, 18531, 21517, 24984, 29010, 33684, 39111, 45413, 52730, 61227, 71092, 82546, 95847, 111290, 129222, 150042, 174218, 202289, 234882, 272728, 316671, 367694, 426938, 495728, 575602, 668346, 776032, 901070, 1046254, 1214830, 1410569, 1637845, 1901741, 2208158, 2563945, 2977058, 3456733];

class MockedNetEthereum is Net::Ethereum {
    method eth_getBlockByNumber(Str $tag) { block_2 }
    method eth_gasPrice { 0x3538a }
}

plan 3;

subtest {
    plan 1 + etalons.elems;

    my $utils = Net::Ethereum::Utils.new;

    for etalons.kv -> $i, $v {
        my $numerator = (889288 + 49873 * 10 * $i);

        is $utils.fake_exponential(:factor(factor), :$numerator, :denominator(denominator)), $v, sprintf("correct exponential %d", $v);
    }

    is $utils.fake_exponential(:factor(factor), :numerator(0), :denominator(denominator)), factor, 'exponent from zero';
}, 'check exponential';

subtest {
    plan 3;

    dies-ok { Net::Ethereum.new.get_base_fee_per_blob_gas(:block({})) }, 'dies on empty block';
    is eth.get_base_fee_per_blob_gas(:block(block_1)), 1, 'base fee per blob gas case no.1';
    is eth.get_base_fee_per_blob_gas(:block(block_2)), 120545, 'base fee per blob gas case no.2';
}, 'blob gas fee';

subtest {
    plan 4;

    my $fees = MockedNetEthereum.new.get_fee_data;

    ok $fees<baseFeePerBlobGas>:exists, 'baseFeePerBlobGas';
    ok $fees<maxFeePerBlobGas>:exists, 'maxBaseFeePerBlobGas';
    is $fees<baseFeePerBlobGas>, 120545, 'value for base fee per blob gas';
    is $fees<maxFeePerBlobGas>, 132600, 'value for max fee per blob gas';
}, 'blob fees in generic fees struct';

done-testing;
