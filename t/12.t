use v6.d;
use Test;

use Net::Ethereum;
use Compress::Bzip2;

constant gasqty = 3000000;

plan 3;

my $unlockpassw = @*ARGS[0] // 'node0';

my $obj = Net::Ethereum.new(
    :api_url('http://127.0.0.1:8501'),
    :unlockpwd($unlockpassw),
    :tx_wait_sec(2),
);

my @textframes;

subtest {
    plan 3;

    my $fh      = 't/data/utf8demo.txt'.IO.open;
    my $textbuf = $fh.read;
    $fh.close;

    my $compressed = compressToBlob($textbuf);

    ok $compressed.bytes < $textbuf.bytes, 'compression';

    my $iterator = 0;

    while $iterator <= $compressed.bytes {
        @textframes.push($compressed.subbuf($iterator, 256));

        $iterator += 256;
    }

    my buf8 $newbuffer .= new;

    for @textframes.values -> $frame {
        $newbuffer.push($frame);
    }

    is $newbuffer.bytes, $compressed.bytes, 'buffer clone bytes';
    is-deeply $newbuffer, $compressed, 'buffer clone payload';
}, 'Prepare buffers';

if $obj {
    my %h = $obj.node_ping;

    if %h<retcode> > -1 {
        subtest {
            plan 3;

            is $obj.personal_unlockAccount, True, 'account is unlocked';

            $obj.abi = './t/solcoutput/StoreBytesArray.abi'.IO.slurp.Str;

            my $bin = sprintf("0x%s", './t/solcoutput/StoreBytesArray.bin'.IO.slurp.Str);
            my $gas = $obj.deploy_contract_estimate_gas($obj.eth_accounts[0], $bin, %(Nil));
            my $tx  = $obj.deploy_contract($obj.eth_accounts[0], $bin, %(Nil), $gas);

            ok($tx ~~ m:i/^ 0x<xdigit>+ $/, sprintf("StoreBytesArray tx=%s", $tx));

            $obj.wait_for_transaction(
                hashes   => @($tx),
                iters    => 15,
                contract => True,
            );

            is($obj.contract_id.defined, True, sprintf("StoreBytesArray addr=%s", $obj.contract_id));
        }, 'StoreBytesArray smart contract deployment';

        subtest {
            plan 5;

            my @hashes;
            my @blockchainframes;

            for @textframes.values -> $frame {
                @hashes.push(
                    $obj.sendTransaction(
                        :account($obj.eth_accounts[0]),
                        :scid($obj.contract_id),
                        :fname('pushRecord'),
                        :fparams({
                            data => $frame,
                        }),
                        :gas(gasqty),
                    )
                );
            }

            is @hashes.elems, @textframes.elems, sprintf("%d transactions are commited", @hashes.elems);

            $obj.wait_for_transaction(:hashes(@hashes));

            my %data = $obj.contract_method_call('getLength', {});

            is %data<length>, @hashes.elems, 'getLength';

            for (0..(%data<length> - 1)) -> $index {
                my %record = $obj.contract_method_call('getRecord', {index => $index});
                @blockchainframes.push(%record<data>);
            }

            is @blockchainframes.elems, @textframes.elems, 'blockchain buffer frames';
            is-deeply @blockchainframes, @textframes, 'valid blockchain buffer';

            my buf8 $blockchaincompbuf .= new;

            for @blockchainframes.values -> $frame {
                $blockchaincompbuf.push($frame);
            }

            is decompressToBlob($blockchaincompbuf).decode, slurp('t/data/utf8demo.txt'), 'text content';
        }, 'Store buffers on blockchain';
    }
    else {
        skip-rest('ethereum node is down');
    }
}
else {
    skip-rest('ethereum object is missed');
}

done-testing;
