use v6.d;
use Test;

use Net::Ethereum;

plan 1;

constant gasqty = 3000000;
my $unlockpassw = @*ARGS[0] // 'node0';

my $obj = Net::Ethereum.new(
    :api_url('http://127.0.0.1:8501'),
    :unlockpwd($unlockpassw),
    :tx_wait_sec(2),
);

if $obj {
    my %h = $obj.node_ping;

    if %h<retcode> > -1 {
        subtest {
            plan 8;

            my %tst_h = $obj.compile_and_deploy_contract(
                :contract_path('PheixAuth.abi'),
                :compile_output_path('./t/solcoutput'),
                :skipcompile(True),
                :constructor_params({
                    _sealperiod => 5,
                    _delta => 60,
                    _seedmod => 999_000_999
                })
            );

            ok %tst_h, 'compile_and_deploy_contract returned Hash';
            ok %tst_h<transactionHash> ~~ m:i/^ 0x<xdigit>**64 $/, 'valid trx hash';

            my %debug = $obj.debug_traceTransaction(:trx(%tst_h<transactionHash>));

            ok %debug, 'valid debug data';
            ok %debug<failed>:exists, '<failed> key found at debug';
            ok %debug<gas>:exists, '<gas> key found at debug';
            ok %debug<returnValue>:exists, '<returnValue> key found at debug';
            ok %debug<structLogs>:exists, '<structLogs> key found at debug';
            ok %debug<structLogs> ~~ Array, 'value of <structLogs> is array';
        }, 'testing debug data retrieval for transaction';
    }
    else {
        skip-rest('ethereum node is down');
    }
}
else {
    skip-rest('ethereum object is missed');
}

done-testing;
