use v6.d;
use Test;

constant test_plan = 1200;

plan test_plan;

use Net::Ethereum;
my $obj = Net::Ethereum.new(api_url => 'http://127.0.0.1:8501');

my Int $tsts  = 999;
my Int $frlen = 16;
my @frames    = txtfile_to_frames('./t/data/utf8demo.txt');

my @phrases;
for (0..$tsts) {
    my @_s = @frames.map({ @frames[@frames.elems.rand.Int] if $_ });
    @phrases.push(@_s[0..10].join);
}

my      @s1;
my      @s2;
my      %params;
my      %tst_h;
my      @tst_a;
my Str  $tst_s;
my Int  $tst_i;
my Bool $tst_b;
my Real $tst_r;

my @d_txt = './t/data/data.txt'.IO.lines;
my @d_hex = './t/data/data.hex'.IO.lines;

for ^@d_txt.elems {
    if @d_txt[$_] && @d_hex[$_] {
        my $mt   = now;
        my $_hex = $obj.string2hex(@d_txt[$_]);
        stats_add( :s(@s1), :i($_), :d('string2hex'), :t($mt) );
        my $_str = $obj.hex2string($_hex);
        stats_add( :s(@s1), :i($_), :d('hex2string'), :t(@s1.tail<now>) );
        is $_hex, @d_hex[$_], 'string2hex built-in data test no.' ~ ($_ + 1);
        is $_str, @d_txt[$_], 'hex2string built-in data test no.' ~ ($_ + 1);
    }
}

for @phrases.kv -> $i, $ph {
    my $mt   = now;
    my $_hex = $obj.string2hex($ph);
    stats_add( :s(@s2), :i($i), :d('string2hex'), :t($mt) );
    my $_str = $obj.hex2string($_hex);
    stats_add( :s(@s2), :i($i), :d('hex2string'), :t(@s2.tail<now>) );
    is $ph, $_str, 'hex2string random data test no.' ~ ($i + 1);
}

done-testing;

sub stats_add(
            :@s!,
    UInt    :$i!,
    Str     :$d!,
    Instant :$t!,
) returns Bool {
    @s.push(
        %(
            iter => $i,
            desc => $d,
            time => (now - $t ),
            now  => now
        )
    );
    True;
}

sub stats_print(
        :@s!,
    Str :$d!
) returns Bool {
    my $s2htime;
    my $h2stime;
    for @s -> %h {
        if ( %h<desc> eq 'string2hex' ) {
            $s2htime += %h<time>;
        }
        else {
            $h2stime += %h<time>;
        }
    }

    ('***INF: ' ~ $d).say;
    printf(
        '%16s : %10f secs' ~ "\n" ~ '%16s : %10f secs' ~ "\n",
        'string2hex',
        $s2htime/(@s.elems/2),
        'hex2string',
        $h2stime/(@s.elems/2)
    );
    True;
}

sub txtfile_to_frames( Str $fname ) returns List {
    my @frames;
    if ( $fname.IO.e ) {
        my $cnt = $fname.IO.slurp;
        my $ff  = ($cnt.chars / $frlen).floor;
        my $hf  = $cnt.chars % $frlen;
        for (0..$ff) {
            if ( $_ < $ff ) {
                @frames.push( $cnt.substr( $_*$frlen, $frlen) );
            } else {
                @frames.push( $cnt.substr( $_*$frlen, $_*$frlen+$hf) );
            }
        }
    }
    @frames;
}

stats_print( :s(@s1), :d('built-in data benchmark') );
stats_print( :s(@s2), :d('random data benchmark') );
