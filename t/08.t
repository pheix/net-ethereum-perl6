use v6.d;
use Test;

use Net::Ethereum;

plan 1;

constant gasqty = 3000000;
my $unlockpassw = @*ARGS[0] // 'node0';

my $obj = Net::Ethereum.new(
    :api_url('http://127.0.0.1:8501'),
    :unlockpwd($unlockpassw),
    :tx_wait_sec(2),
);

if $obj {
    my %h = $obj.node_ping;

    if %h<retcode> > -1 {
        subtest {
            plan 14;

            my @hashes;

            my %tst_h = $obj.compile_and_deploy_contract(
                :contract_path('PheixAuth.abi'),
                :compile_output_path('./t/solcoutput'),
                :skipcompile(True),
                :constructor_params({
                    _sealperiod => 5,
                    _delta => 60,
                    _seedmod => 999_000_999
                })
            );

            ok %tst_h, 'compile_and_deploy_contract returned Hash';

            ok %tst_h<blockHash> ~~ m:i/^ 0x<xdigit>+ $/, 'compile_and_deploy_contract blockHash=' ~ %tst_h<blockHash>;

            ok %tst_h<blockNumber> ~~ m:i/^ 0x<xdigit>+ $/, 'compile_and_deploy_contract blockNumber=' ~ %tst_h<blockNumber>;

            ok %tst_h<contractAddress> ~~ m:i/^ 0x<xdigit>+ $/, 'compile_and_deploy_contract contractAddress=' ~ %tst_h<contractAddress>;

            ok %tst_h<from> eq $obj.eth_accounts[0], 'compile_and_deploy_contract from=' ~ %tst_h<from>;

            ok %tst_h<transactionHash> ~~ m:i/^ 0x<xdigit>+ $/, 'compile_and_deploy_contract transactionHash=' ~ %tst_h<transactionHash>;

            ok %tst_h<status> == 1, 'compile_and_deploy_contract status=' ~ %tst_h<status>;

            my $tst_s = $obj.retrieve_contract( %tst_h<transactionHash> );
            ok $tst_s eq %tst_h<contractAddress>, 'retrieve_contract address=' ~ $tst_s;

            my %tst_pkey_1 = $obj.contract_method_call('getPkey', {});

            ok %tst_pkey_1<publickey>, 'Pkey no.1 is defined';

            ok %tst_pkey_1<publickey> ~~  m:i/^ 0x<xdigit> ** 64 $/, 'Pkey no.1 is hex';

            @hashes.push(
                $obj.sendTransaction(
                    :account($obj.eth_accounts[0]),
                    :scid(%tst_h<contractAddress>),
                    :fname('updateState'),
                    :fparams({}),
                    :gas(gasqty),
                )
            );

            ok @hashes.tail ~~ m:i/^ 0x<xdigit>+ $/, 'sendTransaction updateState hash=' ~ @hashes.tail;

            $obj.wait_for_transaction(:hashes(@hashes));

            my %tst_pkey_2 = $obj.contract_method_call('getPkey', {});

            ok %tst_pkey_2<publickey>, 'Pkey no.2 is defined';

            ok %tst_pkey_2<publickey> ~~  m:i/^ 0x<xdigit> ** 64 $/, 'Pkey no.2 is hex';

            ok %tst_pkey_1<publickey> ne %tst_pkey_2<publickey>, 'Pkeys are different'
        }, 'testing pre-compiled PheixAuth smart contract';
    }
    else {
        skip-rest('ethereum node is down');
    }
}
else {
    skip-rest('ethereum object is missed');
}

done-testing;
