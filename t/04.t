use v6.d;
use Test;

use Net::Ethereum;
my $obj = Net::Ethereum.new(api_url => 'http://127.0.0.1:8501');

my Int  $tsts  = 9999;
my      %params;
my      %tst_h;
my      @tst_a;
my Str  $tst_s;
my Int  $tst_i;
my Bool $tst_b;
my Real $tst_r;
my Str  $contract = ( '0x' ~ '0' x 40 );
my Str  $transact = @*ARGS[0] // %*ENV<TXHASH> // ( '0x' ~ '0' x 64 );

subtest {
    plan 10000;
    for (0..$tsts) {
        my Int $integer = (-10000000..10000000).rand.Int;
        my Str $_m  = $obj.marshal_int( $integer );
        my Int $_um = $obj.unmarshal_int( $_m );
        is $integer, $_um, 'marshal/unmarshal for ' ~ $integer;
    }
}, 'marshal/unmarshal subtest';

%tst_h = $obj.node_ping;
if ( %tst_h<retcode> == -1 ) {
    skip 'ethereum node is down', 37;
    # ( '***INFO: ethereum node is down, skipping ' ~ 37 ~ ' tests from ' ~ $*PROGRAM-NAME ).say;
    # done-testing;
    # exit 0;
}
else {
    subtest {
        plan 45;
        if $transact !~~ m:i/^ <[0x]>+ $/ {
            ok(
                ($transact ~~ m:i/^ 0x<xdigit>**64 $/ ),
                'valid transaction hash ' ~ $transact
            );
            %tst_h = $obj.eth_getTransactionReceipt( $transact );
            ok(
                %tst_h,
                'eth_getTransactionReceipt from ' ~
                ( %tst_h<from> || 'undefined' )   ~
                ' (status: ' ~ ( %tst_h<status> || '-1' ) ~
                ', used gas: ' ~ :16( %tst_h<gasUsed> || '0x0' ) ~ q{)},
            );
            if :16(%tst_h<status>) == 1 {
                $contract = $obj.retrieve_contract( $transact );
                ok(
                    ($contract ~~ m:i/^ 0x<xdigit>**40 $/ ),
                    'valid smart contract addr ' ~ $contract
                );
            }
            else {
                skip 'tx ' ~ $transact  ~ ' not mined', 1;
            }
        } else {
            skip 'no valid hash for eth_getTransactionByHash is passed through input args', 3;
        }
        $tst_s = $obj.getContractMethodId('getMaxId()');
        is $tst_s, '0xd477a712', 'getContractMethodId for getMaxId()';

        $tst_s = $obj.getContractMethodId('getById(uint256)');
        is $tst_s, '0xf4f4d237', 'getContractMethodId for getById(uint256)';

        $tst_s = $obj.getContractMethodId('getCount()');
        is $tst_s, '0xa87d942c', 'getContractMethodId for getCount()';

        $tst_s = $obj.getContractMethodId('init()');
        is $tst_s, '0xe1c7392a', 'getContractMethodId for init()';

        $tst_s = $obj.getContractMethodId('getByIndex(uint256)');
        is $tst_s, '0x2d883a73', 'getContractMethodId for getByIndex(uint256)';

        $tst_s = $obj.getContractMethodId('removeById(uint256)');
        is $tst_s, '0x6fe667cb', 'getContractMethodId for removeById(uint256)';

        $obj = Net::Ethereum.new(api_url => 'http://127.0.0.1:8501');

        $obj.abi = './t/solcoutput/BigBro.abi'.IO.slurp.Str;
        is $obj.abi, './t/solcoutput/BigBro.abi'.IO.slurp.Str, 'abi check setter';

        %tst_h = $obj.get_function_abi('setById');
        is %tst_h<name>, 'setById', 'get_function_abi for ' ~ %tst_h<name> ~ ': inputs=' ~ %tst_h<inputs>.elems ~ ', outputs=' ~ %tst_h<outputs>.elems;

        %tst_h = $obj.get_constructor_abi;
        ok !%tst_h, 'no constructor in BigBro abi';

        my $feedata = $obj.get_fee_data;

        is $feedata.keys.elems, 6, 'fee data';

        for $feedata.kv -> $key, $value {
            next unless $value.defined;

            ok $value > 0, sprintf("%s = %d", $key, $value);
        }

        my @marshals = (
            %( int => 1000, marshal => '00000000000000000000000000000000000000000000000000000000000003e8' ),
            %( int => 32300, marshal => '0000000000000000000000000000000000000000000000000000000000007e2c' ),
            %( int => 0, marshal => '0000000000000000000000000000000000000000000000000000000000000000' ),
            %( int => -10, marshal => 'fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6' ),
            %( int => -29937, marshal => 'ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8b0f' ),
            %( int => 1, marshal => '0000000000000000000000000000000000000000000000000000000000000001' ),
            %( int => -19929, marshal => 'ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb227' ),
            %( int => 65535, marshal => '000000000000000000000000000000000000000000000000000000000000ffff' ),
            %( int => 9939, marshal => '00000000000000000000000000000000000000000000000000000000000026d3' ),
            %( int => -77399773993, marshal => 'ffffffffffffffffffffffffffffffffffffffffffffffffffffffedfa9d2cd7' ),
            %( int => -65565, marshal => 'fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeffe3' ),
            %( int => 21365565, marshal => '000000000000000000000000000000000000000000000000000000000146033d' ),
            %( int => 565, marshal => '0000000000000000000000000000000000000000000000000000000000000235' ),
            %( int => 1982, marshal => '00000000000000000000000000000000000000000000000000000000000007be' ),
        );

        $obj = Net::Ethereum.new(api_url => 'http://127.0.0.1:8501');
        for @marshals -> %h {
            $tst_s = $obj.marshal_int( %h<int> );
            is $tst_s, %h<marshal>, 'marshal_int(' ~ %h<int> ~ ')';
            $tst_i = $obj.unmarshal_int( %h<marshal> );
            is $tst_i, %h<int>, 'unmarshal_int(' ~ %h<marshal> ~ ')';
        }
    }, 'getContractMethodId and deep marshalling subtest';
}

done-testing;
