use v6.d;
use Test;

constant test_plan = 12;
use Net::Ethereum;
my $obj   = Net::Ethereum.new(api_url => 'http://127.0.0.1:8501');
my %tst_h = $obj.node_ping;

subtest {
     plan 1;
     ok $obj, 'Net::Ethereum class';
}, 'Net::Ethereum constructor';

if ( %tst_h<retcode> == -1 ) {
    skip 'ethereum node is down', test_plan;
}
else {
    subtest {
        plan test_plan;

        %tst_h = $obj.node_ping;
        ok %tst_h<retcode> == 0, 'node_ping: Ethereum node is active';
        my Int $_prt = 18501;
        my $_obj =
            Net::Ethereum.new(api_url => 'http://127.0.0.1:'~$_prt);
        %tst_h = $_obj.node_ping;
        ok %tst_h<retcode> == -1,
            'node_ping: Ethereum node is down at port ' ~ $_prt;

        my Str $tst_s = $obj.web3_clientVersion;
        ok $tst_s, 'web3_clientVersion: ' ~ $tst_s;

        $tst_s = $obj.web3_sha3('0x68656c6c6f20776f726c64');
        ok $tst_s, 'web3_sha3: ' ~ $tst_s;

        my Int $tst_i = $obj.net_version;
        ok $tst_i, 'net_version: ' ~ $tst_i;

        $tst_i = $obj.net_listening;
        ok ($tst_i == 0 || $tst_i == 1), 'net_listening: ' ~ $tst_i;

        $tst_i = $obj.net_peerCount;
        ok ( $tst_i >= 0 ), 'net_peerCount: ' ~ $tst_i;

        # $tst_i = $obj.eth_protocolVersion;
        # ok $tst_i, 'eth_protocolVersion: ' ~ $tst_i;

        my Bool $tst_b = $obj.eth_syncing;
        ok ( $tst_b == True || $tst_b == False ), 'eth_syncing: ' ~ $tst_b;

        $tst_s = $obj.eth_coinbase;
        ok $tst_s, 'eth_coinbase: ' ~ $tst_s;

        $tst_i = $obj.eth_mining;
        ok ($tst_i == 0 || $tst_i == 1), 'eth_mining: ' ~ $tst_i;

        # it's bug, will be bringed back asap: https://github.com/ethereum/go-ethereum/issues/22735#issuecomment-829058506
        # $tst_i = $obj.eth_hashrate;
        # ok ( $tst_i >= 0 ), 'eth_hashrate: ' ~ $tst_i;

        $tst_i = $obj.eth_gasPrice;
        ok ( $tst_i >= 0 ), 'eth_gasPrice: ' ~ $tst_i;

        my @tst_a = $obj.eth_accounts;
        ok @tst_a.elems > 0, 'eth_accounts size=' ~ @tst_a.elems ~ ', a[0]=' ~ @tst_a[0];

    }, 'Net::Ethereum JSON RPC API';
}

done-testing;
