use v6.d;
use Test;

use Net::Ethereum;

plan 2;

constant gasqty = 3000000;
constant iters  = 5;
constant amount = 15_200;


my $unlockpassw = @*ARGS[0] // 'node0';

my $obj = Net::Ethereum.new(
    :api_url('http://127.0.0.1:8501'),
    :unlockpwd($unlockpassw),
    :tx_wait_sec(10),
);

if $obj {
    my %h = $obj.node_ping;

    if %h<retcode> > -1 {
        subtest {
            my $sender     = $obj.get_account(:index(0));
            my $receipient = $obj.get_account(:index(1));

            ok $obj.personal_unlockAccount, 'account is unlocked';

            for (1..iters) {
                my $balance         = $obj.eth_getBalance($sender, 'latest');
#               my $fixed_gas_price = $obj.eth_gasPrice();

                my $trx = {
                    from  => $sender,
                    to    => $receipient,
                    value => '0x' ~ amount.base(16),
                    data  => $obj.personal_sign(:message($obj.string2hex(sprintf("%d", $balance - amount)))),
#                   gasPrice => '0x' ~ $fixed_gas_price.base(16),
                };

#               my $fee = $obj.eth_estimateGas($trx) * $fixed_gas_price;
                my $txhash = $obj.eth_sendTransaction($trx);

                is $obj.wait_for_transaction(:hashes([$txhash])).elems, 1, sprintf("transaction %s is mined", $txhash);

                my %receipt = $obj.eth_getTransactionReceipt($txhash);
                my $fee     = %receipt<cumulativeGasUsed> * %receipt<effectiveGasPrice>;

                my $upd_balance = $obj.eth_getBalance($sender, 'latest');

                is $upd_balance, $balance - amount - $fee, sprintf("sender %s balance change %d -> %d", $sender, $balance, $upd_balance);
            }
        }, 'send tokens to account';

        subtest {
            ok $obj.personal_unlockAccount, 'account is unlocked';

            my %tst_h = $obj.compile_and_deploy_contract(
                :contract_path('PheixAuth.abi'),
                :compile_output_path('./t/solcoutput'),
                :skipcompile(True),
                :constructor_params({
                    _sealperiod => 5,
                    _delta => 60,
                    _seedmod => 999_000_999
                })
            );

            ok %tst_h && %tst_h<transactionHash> ~~ m:i/^ 0x<xdigit>**64 $/, 'smart contract deployment';

            my $sender     = $obj.get_account(:index(0));
            my $receipient = $obj.retrieve_contract(%tst_h<transactionHash>);

            for (1..iters) {
                my $sender_balance     = $obj.eth_getBalance($sender, 'latest');
                my $receipient_balance = $obj.eth_getBalance($receipient, 'latest');
#               my $fixed_gas_price    = $obj.eth_gasPrice();

                my $trx = {
                    from     => $sender,
                    to       => $receipient,
                    value    => '0x' ~ amount.base(16),
#                   gasPrice => '0x' ~ $fixed_gas_price.base(16),
                    data     => $obj.personal_sign(:message($obj.string2hex(sprintf("%d", $receipient_balance + amount)))),
                };

#               my $fee    = $obj.eth_estimateGas($trx) * $fixed_gas_price;
                my $txhash = $obj.eth_sendTransaction($trx);

                is $obj.wait_for_transaction(:hashes([$txhash])).elems, 1, sprintf("transaction %s is mined", $txhash);

                my %receipt = $obj.eth_getTransactionReceipt($txhash);
                my $fee     = %receipt<cumulativeGasUsed> * %receipt<effectiveGasPrice>;

                my $upd_sender_balance     = $obj.eth_getBalance($sender, 'latest');
                my $upd_receipient_balance = $obj.eth_getBalance($receipient, 'latest');

                is $upd_sender_balance, $sender_balance - amount - $fee, sprintf("sender %s balance change %d -> %d", $sender, $sender_balance, $upd_sender_balance);
                is $upd_receipient_balance, $receipient_balance + amount, sprintf("receipient %s balance change %d -> %d", $receipient, $receipient_balance, $upd_receipient_balance);

                my $mined_trx = $obj.eth_getTransactionByHash($txhash);

                is $obj.personal_ecRecover(:message($obj.string2hex(sprintf("%d", $upd_receipient_balance))), :signature($mined_trx<input>)), $sender, 'balance signature';
            }
        }, 'send tokens to smart contract';
    }
    else {
        skip-rest('ethereum node is down');
    }
}
else {
    skip-rest('ethereum object is missed');
}

done-testing;
