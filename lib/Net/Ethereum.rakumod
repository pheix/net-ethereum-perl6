unit class Net::Ethereum;

use JSON::Fast;
use Net::Ethereum::Utils;
use Node::Ethereum::Keccak256::Native;

use HTTP::UserAgent:ver<1.1.58+>:auth<zef:knarkhov>;
use HTTP::Request:ver<1.1.58+>:auth<zef:knarkhov>;
use HTTP::Cookies:ver<1.1.58+>:auth<zef:knarkhov>;

constant MIN_BASE_FEE_PER_BLOB_GAS     = 1;
constant TARGET_BLOB_GAS_PER_BLOCK     = 393216;
constant GAS_PER_BLOB                  = 2**17;
constant BLOB_BASE_FEE_UPDATE_FRACTION = 3338477;

has      @.accounts;
has Str  $.unlockpwd;
has Any  $.ua = HTTP::UserAgent.new(:cookies(HTTP::Cookies.new));

has Str  $.api_url         is rw;
has Str  $.abi             is rw;
has Str  $.contract_id     is rw;
has UInt $.tx_wait_sec     is default(15) is rw;
has Int  $.tx_wait_iters   is default(20) is rw;
has Bool $.keepalive       is default(False) is rw;
has Bool $.show_progress   is default(False) is rw;
has Str  $.solidity        is default('/usr/local/bin/solc');
has Str  $.conn_name       is default('local-ethereum-node');
has Bool $.debug           is default(False);
has Int  $.return_raw_data is default(0);
has Bool $.cleanup         is default(True);
has Str  $.evmver          is default('paris');

has Rat  $!gaspercent is default(0.25);
has UInt $!sessionid  is default(0);
has UInt $!maxpriorityfee is default(1_500_000_000);
has Net::Ethereum::Utils $!ethutils = Net::Ethereum::Utils.new;

class X is Exception {
    has $.payload is default('smth went wrong') is rw;

    method message {
        sprintf("%s: %s", self.^name, $!payload);
    }
};

class X::ParseABI is X {};
class X::InvalidBlock is X {};
class X::InvalidTag is X {};
class X::InvalidData is X {};
class X::InvalidPassword is X {};
class X::InvalidTopics is X {};
class X::JSONAPI is X {};
class X::JSONAPI::URL is X {};
class X::JSONAPI::InvalidResponse is X::JSONAPI {};
class X::JSONAPI::InvalidResponseStatus is X::JSONAPI {};
class X::JSONAPI::ParseResponse is X::JSONAPI {};
class X::JSONAPI::InvalidID is X::JSONAPI {
    submethod BUILD(:$expected!, :$got!) {
        self.payload = sprintf("got id %s while expecting %s", ~$got, ~$expected);
    }
};

method get_account(UInt :$index = 0) {
    if !@!accounts[$index] {
        my @accs = self.eth_accounts;

        @!accounts[$index] = @accs[$index]
            if @accs && @accs[$index] ~~ m:i/^ 0x<xdigit>**40 $/;
    }

    return @!accounts[$index];
}

method !prepare_request(Hash $query) returns HTTP::Request {
    die Net::Ethereum::X::JSONAPI::URL.new(:payload('no endpoint is setup')) unless $!api_url;

    my $request = HTTP::Request.new( POST => $!api_url );
    $request.header.field(
        Content-Type => 'application/json',
        Connection   => $!keepalive ?? 'Keep-Alive' !! 'close',
    );

    $request.add-content(to-json($query));
    # ( '***DEBUG > request: ' ~ $request.perl ).say if $!debug;
    # ( '***DEBUG >  ' ~ "\n" ~ $request).say if $!debug;

    return $request;
}

method !check_block($block) returns Str {
    my Str $ret;

    if $block ~~ UInt {
        $ret = ( '0x' ~ ( $block.base(16) // 0 ) );
    } elsif ( $block ~~ Str ) {
        if ( $block eq 'latest' || $block eq 'earliest' || $block eq 'pending' ) {
            $ret = $block;
        }
        else {
            die Net::Ethereum::X::InvalidBlock.new(:payload('block should be one of latest/earliest/pending'));
        }
    } else {
        die Net::Ethereum::X::InvalidBlock.new(:payload('block is not type of UInt or Str'));
    }

    return $ret;
}

method check_ua_keepalive returns Bool {
    return (
            $!ua.^find_method('check_connection').defined &&
            $!ua.^find_method('store_connection').defined &&
            $!ua.^find_method('close_connection').defined
    );
}

method finalize_request returns Bool {
    ( self.check_ua_keepalive && $!keepalive ) ??
        $!ua.close_connection( name => $!conn_name ) !!
            False;
}

method node_request(Hash $query) returns Hash {
    my %ret;
    my $response;

    $query<id> = ++$!sessionid;

    my $req = self!prepare_request($query);
    if $!keepalive {
        if self.check_ua_keepalive {
            if !$!ua.check_connection(:name($!conn_name)) {
                $!ua.store_connection(
                    :name($!conn_name),
                    :conn($!ua.get-connection($req)),
                );
            }
            $response = $!ua.request($req, conn_name => $!conn_name);
        }
        else {
            $!keepalive = False;
            $response   = $!ua.request($req);
        }
    }
    else {
        $response = $!ua.request($req);
    }

    if $response.code == 200 {
        my Str $response_cnt =
            $response.content ??
                $response.content !!
                    $response.content.gist;

        ('>>>' ~ $req ~ "\n===\n" ~ '<<<' ~ $response_cnt).say if $!debug;

        try {
            %ret = from-json($response_cnt);

            CATCH {
                default {
                    my $e = .message;

                    die Net::Ethereum::X::JSONAPI::ParseResponse.new(:payload($e));
                }
            };
        }

        die Net::Ethereum::X::JSONAPI::InvalidID.new(:expected($query<id>), :got(%ret<id> // 0))
                unless $query<id> === (%ret<id> // 0) ;

        if (%ret<error>:!exists) && (%ret<result>:!exists) {
            ('***DEBUG > %ret: ' ~ %ret.gist).say if $!debug;

            die Net::Ethereum::X::JSONAPI::InvalidResponse.new(:payload($response_cnt));
        }
        else {
            if %ret<error> {
                die Net::Ethereum::X::JSONAPI.new(:payload(%ret.gist));
            }
        }
    }
    else {
        die Net::Ethereum::X::JSONAPI::InvalidResponseStatus.new(:payload($response.status-line));
    }

    return %ret;
}

method node_ping returns Hash {
    my %rc      = retcode => 0, retmess => "success", rethash => Nil;
    my $rq      = %(
        jsonrpc => "2.0",
        method  => "web3_clientVersion",
        params  => []
    );
    try {
        %rc<rethash> = self.node_request($rq)<result>;
        CATCH {
            default {
                %rc<retcode> = -1;
                %rc<retmess> =
                    "Ethereum node is down - " ~ ( .Str ) ~ ". " ~
                    "Source: " ~ &?ROUTINE.name ~ " at line "  ~
                    .backtrace[1].line;
            }
        }
    }
    %rc;
}

method web3_clientVersion returns Str {
    my $rq = %( jsonrpc => "2.0", method => "web3_clientVersion", params => []);
    self.node_request($rq)<result>;
}

method web3_sha3(Str $val) returns Str {
    my $keccak256 = Node::Ethereum::Keccak256::Native.new;

    return self.buf2hex($keccak256.keccak256(:msg(self.hex2buf($val)))).lc;
}

method net_version returns Int {
    my $rq = %( jsonrpc => "2.0", method => "net_version", params => []);
    self.node_request($rq)<result>.Int;
}

method net_listening returns Int {
    my $rq = %( jsonrpc => "2.0", method => "net_listening", params => []);
    self.node_request($rq)<result> ?? 1 !! 0;
}

method net_peerCount returns Int {
    my $rq = %( jsonrpc => "2.0", method => "net_peerCount", params => []);
    self.node_request($rq)<result>.Int;
}

method eth_syncing returns Bool {
    my $rq = %( jsonrpc => "2.0", method => "eth_syncing", params => []);
    my Int $rc = self.node_request($rq)<result>.Int;
    ( !$rc ?? False !! True )
}

method eth_coinbase returns Str {
    my $rq = %( jsonrpc => "2.0", method => "eth_coinbase", params => []);
    self.node_request($rq)<result>;
}

method eth_mining returns Int {
    my $rq = %( jsonrpc => "2.0", method => "eth_mining", params => []);
    self.node_request($rq)<result> ?? 1 !! 0;
}

method eth_hashrate returns Int {
    my $rq = %( jsonrpc => "2.0", method => "eth_hashrate", params => []);
    self.node_request($rq)<result>.Int;
}

method eth_gasPrice returns Int {
    my $rq = %( jsonrpc => "2.0", method => "eth_gasPrice", params => []);
    self.node_request($rq)<result>.Int;
}

method eth_accounts returns Array {
    my $rq = %( jsonrpc => "2.0",method => "eth_accounts", params => []);
    self.node_request($rq)<result>.Array;
}

method eth_blockNumber returns Int {
    my $rq = %( jsonrpc => "2.0", method => "eth_blockNumber", params => []);
    self.node_request($rq)<result>.Int;
}

method eth_getBalance(Str $account, Str $tag) returns Int {
    if !( $tag eq 'latest' || $tag eq 'earliest' || $tag eq 'pending' ) {
        if !$tag || !( $tag ~~ m:i/ 0x<xdigit>+ / ) || !( :16($tag) ~~ Int ) {
            die Net::Ethereum::X::InvalidTag.new(:payload('at eth_getBalance: ' ~ $tag));
        }
    }
    my $rq = %( jsonrpc => "2.0", method => "eth_getBalance", params => [ $account, $tag ]);
    self.node_request($rq)<result>.Int;
}

method eth_getTransactionCount(
    Str :$data!,
    Str :$tag
) returns Int {
    if $tag {
        if !($tag eq 'latest' || $tag eq 'earliest' || $tag eq 'pending') {
            if !($tag ~~ m:i/^ 0x<xdigit>+ $/) || !($tag.Int > 0) {
                die Net::Ethereum::X::InvalidTag.new(:payload('at eth_getTransactionCount: ' ~ $tag));
            }
        }
    }

    die Net::Ethereum::X::InvalidTag.new(:payload('at eth_getTransactionCount: ' ~ $data))
        if $data !~~ m:i/^ 0x<xdigit>+ $/;

    my $rq = %( jsonrpc => "2.0", method => "eth_getTransactionCount", params => [ $data, $tag // 'pending']);

    self.node_request($rq)<result>.Int;
}

method eth_getTransactionByHash(Str $hash) returns Hash {
    my $rq = { jsonrpc => "2.0", method => "eth_getTransactionByHash", params => [ ~($hash) ]};
    self.node_request($rq)<result> || %( Nil );
}

method eth_getBlockByNumber($block, Bool $full_tx_obj?) returns Hash {
    my %ret;
    my Str $block_param = self!check_block( $block );
    my $rq = {
        jsonrpc => "2.0",
        method  => "eth_getBlockByNumber",
        params  => [
            $block_param,
            $full_tx_obj // False
        ]
    };
    self.node_request($rq)<result> || %( Nil );
}

method eth_getBlockByHash(Str $blockhash, Bool $full_tx_obj?) returns Hash {
    if !($blockhash ~~ m:i/^ 0x<xdigit>**64 $/) {
        die Net::Ethereum::X::InvalidBlock.new(:payload('block hash length: not 32 bytes'));
    }
    my $rq = {
        jsonrpc => "2.0",
        method  => "eth_getBlockByHash",
        params  => [
            $blockhash,
            $full_tx_obj // False
        ]
    };
    self.node_request($rq)<result> || %( Nil );
}

method eth_getBlockTransactionCountByHash(Str $blockhash) returns Int {
    my $rq  = %( jsonrpc => "2.0", method => "eth_getBlockTransactionCountByHash", params => [ ~($blockhash) ]);
    my $cnt = self.node_request($rq)<result>;
    ( $cnt ?? $cnt.Int !! 0 );
}

method eth_getCode(Str $addr, Str $tag) returns Str {
    if !( $tag eq 'latest' || $tag eq 'earliest' || $tag eq 'pending' ) {
        if !( $tag ~~ m:i/ 0x<xdigit>+ / ) || !( :16($tag) ~~ Int ) {
            die Net::Ethereum::X::InvalidTag.new(:payload('at eth_getCode: ' ~ $tag));
        }
    }
    my $rq = %(
        jsonrpc => "2.0",
        method  => "eth_getCode",
        params => [ $addr, $tag ]
    );
    self.node_request($rq)<result>.Str;
}

method eth_getTransactionReceipt(Str $hash) returns Hash {
    my $rq = %( jsonrpc => "2.0", method => "eth_getTransactionReceipt", params => [ ~($hash) ]);
    self.node_request($rq)<result> || %( Nil );
}



method eth_call(%params) returns Str {
    my $rq = %(jsonrpc => "2.0", method => "eth_call", params => [%params, "latest"]);
    self.node_request($rq)<result>.Str;
}

method eth_estimateGas(%params) returns Int {
    my $p = {from => %params<from> // self.get_account, to => %params<to>, data => %params<data>};

    $p<to>:delete unless %params<to>;

    my $rq = %(
        jsonrpc => "2.0",
        method  => "eth_estimateGas",
        params  => [$p],
    );

    self.node_request($rq)<result>.Int;
}

method eth_sendTransaction(Hash $params) returns Str {
    my $rq = %(
        jsonrpc => "2.0",
        method => "eth_sendTransaction",
        params => [$params]
    );

    my %h = self.node_request($rq);

    sprintf("***DEBUG > tx: %s", %h<result>.Str).say if $!debug;

    return %h<result>.Str;
}

method eth_signTransaction(
    Str  :$from!,
    Str  :$to!,
    UInt :$gas,
    UInt :$gasprice,
    Str  :$data!,
    UInt :$nonce,
    UInt :$value,
    UInt :$maxfeepergas,
    UInt :$maxpriorityfeepergas,
) returns Hash {
    my $feesdata = self.get_fee_data(:$maxpriorityfeepergas, :$maxfeepergas);

    my $txobj = {
        from     => $from,
        to       => $to,
        gas      => '0x' ~ ($gas || 90000).base(16),
        data     => $data,
        nonce    => '0x' ~ ($nonce || (self.eth_getTransactionCount(:data($from)))).base(16),
    };

    $txobj<value> = sprintf("0x%s", $value.base(16)) if $value;

    if ($feesdata<maxFeePerGas> && $feesdata<maxPriorityFeePerGas> && !$gasprice) {
        $txobj<maxFeePerGas>         = sprintf("0x%s", ($feesdata<maxFeePerGas>).base(16));
        $txobj<maxPriorityFeePerGas> = sprintf("0x%s", ($feesdata<maxPriorityFeePerGas>).base(16));
    }
    else {
        $txobj<gasPrice> = sprintf("0x%s", $gasprice.base(16));
    }

    my $rq = %(
        jsonrpc => "2.0",
        method => "eth_signTransaction",
        params => [$txobj],
    );

    my %h = self.node_request($rq);

    sprintf("***DEBUG > signing tx with data: %s", $txobj.gist).say if $!debug;
    sprintf("***DEBUG > signed tx: %s", %h<result>.gist).say if $!debug;

    return %h<result>:exists ?? %h<result> !! Hash.new;
}

method eth_sendRawTransaction(Str :$data!) returns Str {
    my $rq = %(
        jsonrpc => "2.0",
        method => "eth_sendRawTransaction",
        params => [$data]
    );

    die Net::Ethereum::X::InvalidData.new(:payload('at eth_sendRawTransaction: ' ~ $data))
        if $data !~~ m:i/^ 0x<xdigit>+ $/;

    my %h = self.node_request($rq);

    ( '***DEBUG > tx: ' ~ %h<result>.Str ).say if $!debug;

    %h<result>.Str;
}

method eth_getLogs($filter) returns List {
    my $rq = %(
        jsonrpc => "2.0",
        method  => "eth_getLogs",
        params  => [$filter],
    );

    return self.node_request($rq)<result> || Empty;
}

method eth_maxPriorityFeePerGas returns UInt {
    my $rq = %(
        jsonrpc => "2.0",
        method  => "eth_maxPriorityFeePerGas",
    );

    my $maxpriorityfeepergas = 0;

    try {
        $maxpriorityfeepergas = self.node_request($rq)<result>.UInt;

        CATCH {
            default {
                my $e = .message;

                sprintf("eth_maxPriorityFeePerGas is unsupported: %s", $e).say if $!debug;
            }
        };
    }

    return $maxpriorityfeepergas || $!maxpriorityfee;
}

method eth_createAccessList(Hash :$transaction!, Cool :$blocknumberortag = 'latest') returns Hash {
    my $rq = %(
        jsonrpc => "2.0",
        method  => "eth_createAccessList",
        params  => [$transaction, $blocknumberortag],
    );

    my $access_list = Hash.new;

    try {
        $access_list = self.node_request($rq)<result>;

        CATCH {
            default {
                my $e = .message;

                sprintf("eth_createAccessList is failed or unsupported: %s", $e).say if $!debug;
            }
        };
    }

    return $access_list;
}

method personal_newAccount(Str :$password) returns Str {
    die Net::Ethereum::X::InvalidPassword.new(:payload('at personal_newAccount'))
        unless $password && $password.chars;

    my $rq = %(
        jsonrpc => "2.0",
        method  => "personal_newAccount",
        params  => [$password],
    );

    return self.node_request($rq)<result>.Str;
}

method personal_unlockAccount(Str :$account, Str :$password, Int :$duration = 300) returns Bool {
    my $rq = %(
        jsonrpc => "2.0",
        method  => "personal_unlockAccount",
        params  =>
            [
                $account  // self.get_account,
                $password // $!unlockpwd,
                $duration
            ]
    );

    return self.node_request($rq)<result>.Bool;
}

method personal_lockAccount(Str :$account) returns Bool {
    my $rq = %(
        jsonrpc => "2.0",
        method  => "personal_lockAccount",
        params  =>
            [
                $account  // self.get_account,
            ]
    );

    return self.node_request($rq)<result>.Bool;
}

method personal_listWallets(Str :$account, Str :$password) returns Array {
    my @ret;

    my $rq = %(
        jsonrpc => "2.0",
        method  => "personal_listWallets",
        params  => []
    );

    my %h = self.node_request($rq);

    @ret := %h<result> if %h<result>:exists;

    return @ret;
}

method personal_sign(
    Str :$message!,
    Str :$account,
    Str :$password
) returns Str {
    my $rq = %(
        jsonrpc => "2.0",
        method  => "personal_sign",
        params  =>
            [
                $message,
                $account  // self.get_account,
                $password // $!unlockpwd
            ]
    );

    return self.node_request($rq)<result>.Str;
}

method personal_ecRecover(Str :$message!, Str :$signature!) returns Str {
    my $rq = %(
        jsonrpc => "2.0",
        method  => "personal_ecRecover",
        params  => [$message, $signature]
    );

    return self.node_request($rq)<result>.Str;
}

method debug_traceTransaction(Str :$trx!) returns Hash {
    return {
        error => sprintf("invalid trx hash: %s", $trx),
    } unless $trx && $trx ~~ m:i/^ 0x<xdigit>**64 $/;

    my $rq = %(
        jsonrpc => "2.0",
        method  => "debug_traceTransaction",
        params  => [$trx]
    );

    self.node_request($rq)<result> // {};
}

method wei2ether(UInt $wei) returns Real {
    ($wei / 1000000000000000000);
}

method pack_filter_params(
    :$fromblock,
    :$toblock,
    Str  :$address,
    List :$topics!
) returns Hash {
    my %ret =
        fromBlock => 'earliest',
        toBlock   => 'latest',
        topics    => []
    ;
    %ret<fromBlock> = self!check_block( $fromblock ) if $fromblock;
    %ret<toBlock>   = self!check_block( $toblock )   if $toblock;
    %ret<address>   =
        $address if $address.defined && $address ~~ m:i/^ 0x<xdigit>**40 $/;

    if $topics.elems >= 1 {
        for $topics.values -> $t {
            if $t !~~ m:i/^ 0x<xdigit>**64 $/ {
                die Net::Ethereum::X::InvalidTopics.new(:payload('at pack_filter_params'))
            }
        }

        %ret<topics> = $topics;
    }

    return %ret;
}

method hex2string(Str $hex) returns Str {
    my @reencoded;
    my @codes = ( $hex ~~ m:g/../ ).map({ :16($_.Str) if $_ ne '0x' });
    my $blob = Blob.new( @codes );
    $blob.decode;
}

method string2hex(Str $s) returns Str {
    my @reencoded;
    my @encoded = $s.encode.map({ $_ });
    for @encoded -> $data {
        my $hex = $data.base(16);
        @reencoded.push( ( $data < 16 ) ?? '0' ~ $hex !! $hex );
    }
    '0x' ~ @reencoded.join(q{});
}

method buf2hex(Buf[uint8] $buffer) returns Str {
    my @reencoded;
    for $buffer.List -> $data {
        my $hex = $data.base(16);
        @reencoded.push( ( $data < 16 ) ?? '0' ~ $hex !! $hex );
    }
    '0x' ~ @reencoded.join(q{});
}

method hex2buf(Str $hex) returns Buf[uint8] {
    my @reencoded;
    my @codes = ( $hex ~~ m:g/../ ).map({ :16($_.Str) if $_ ne '0x' });
    Buf[uint8].new(@codes);
}

method getContractMethodId(Str $method_name) returns Str {
    my $method_name_hex = self.string2hex($method_name);
    my $hash = self.web3_sha3($method_name_hex);
    substr( $hash, 0, 10 );
}

method get_function_abi(Str $func) returns Hash {
    my %ret;
    my $abi;

    die Net::Ethereum::X.new(:payload('abi attr is not set')) unless $!abi;

    try {
        $abi = from-json($!abi);

        CATCH {
            default {
                my $e = .message;

                die Net::Ethereum::X::ParseABI.new(:payload($e));
            }
        };
    }

    for @$abi -> %_ret {
        if %_ret<type> eq 'function' && %_ret<name> eq $func {
            %ret = %_ret;
            last;
        }
    }

    return %ret;
}

method get_constructor_abi returns Hash {
    my %ret;
    my $abi;

    die Net::Ethereum::X.new(:payload('abi attr is not set')) unless $!abi;

    try {
        $abi = from-json($!abi);

        CATCH {
            default {
                my $e = .message;

                die Net::Ethereum::X::ParseABI.new(:payload($e));
            }
        };
    }

    for @$abi -> %_ret {
        if %_ret<type> eq 'constructor' {
            %ret = %_ret;
            last;
        }
    }

    return %ret;
}

method marshal_int(Int $marshal) returns Str {
    my Int $_in = $marshal;
    my Int $filler_size;
    my Str $bint_hex;
    my Str $filler_char;
    if $_in < 0 {
        $_in .= abs;
        $_in +^= 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
        $_in++;
        $filler_char = 'f';
    }
    else {
        $filler_char = '0';
    }
    $bint_hex = $_in.base(16).Str;
    $filler_size = 64 - $bint_hex.chars;
    return ( $filler_char x $filler_size ) ~ $bint_hex.lc;
}

method unmarshal_int(Str $unmarshal) returns Int {
    my $u_int = :16($unmarshal);
    my $neg = :16($unmarshal);
    if ( $neg +& 0x8000000000000000000000000000000000000000000000000000000000000000 ) {
        $u_int +^= 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
        $u_int++;
        $u_int *= -1;
    }
    $u_int;
}

method marshal(Str $fname, Hash $fparams) returns Str {
    my %fabi;
    if $fname eq 'constructor' {
        %fabi = self.get_constructor_abi;
    }
    else {
        %fabi = self.get_function_abi( $fname );
    }

    return '0x0' unless %fabi && %fabi.keys;

    my $inputs = %fabi<inputs>;
    my Int $current_out_param_position = 0;
    my Int $out_param_array_counter    = 0;
    my Str $param_types_list;
    my Str $encoded_arguments;
    my Str $add_hunk;
    my Str $raw;
    my @out_param_array;

    # First pass
    for @$inputs -> %out_param {
        my Str $cur_pname  = %out_param<name>;
        my Str $cur_ptype  = %out_param<type>;
        $param_types_list ~= $cur_ptype ~ q{,};

        if $cur_ptype eq 'bool' || $cur_ptype ~~ m:i/^ u?int / {
            $add_hunk=self.marshal_int( $fparams{$cur_pname} );
            @out_param_array[$out_param_array_counter] = $add_hunk;
        }
        elsif $cur_ptype eq 'address' {
            $add_hunk = sprintf( "%064s", substr($fparams{$cur_pname}, 2) );
            @out_param_array[$out_param_array_counter] = $add_hunk;
        }
        elsif ($cur_ptype eq 'string' || $cur_ptype eq 'bytes') {
            $current_out_param_position += 64;
            # replace with data hunk offset into second pass
            @out_param_array[$out_param_array_counter] =
                $cur_ptype eq 'string' ?? $fparams{$cur_pname}.chars !!
                    $fparams{$cur_pname}.bytes;
        }
        else {
            die Net::Ethereum::X.new(:payload('marshal does not support type: ' ~ $cur_ptype));
        }
        $out_param_array_counter++;
        $current_out_param_position += 64;
    }

    ( '***DEBUG > : {$out_param_array_counter=' ~ $out_param_array_counter ~ q{\}} ~ "\n" ~ @out_param_array.join("\n") ).say if $!debug;

    # Second pass
    my Int $var_data_offset_index = $out_param_array_counter;
    my Int $var_data_offset = $var_data_offset_index * 32;
    my Int $array_index         = 0;
    my Int $second_pass_counter = 0;

    for @$inputs -> %out_param {
        my Str $cur_pname  = %out_param<name>;
        my Str $cur_ptype  = %out_param<type>;
        if $cur_ptype eq 'address' || $cur_ptype eq 'bool' || $cur_ptype ~~ m:i/^ u?int / {
            $array_index++;
        }
        elsif ($cur_ptype eq 'string' || $cur_ptype eq 'bytes') {
            my Str $hunk;
            my Str $fparam;
            if $cur_ptype eq 'string' {
                $fparam = $fparams{$cur_pname};
                $hunk   = self.string2hex($fparam);
            }
            else {
                $fparam = $hunk = self.buf2hex($fparams{$cur_pname});
            }
            my Int $hsize   = $hunk.chars;
            my Int $number_of_32b_blocks = ($hsize / 64).floor;
            my Int $part = $hsize % 64;
            my Str $repeater = '0' x (64 - $part + 2);
            my Str $hunk_appended = substr($hunk ~ $repeater, 2);

            ( '***DEBUG > : ' ~
                "\$array_index=$array_index\n" ~
                "\$var_data_offset=$var_data_offset\n" ~
                "\$fparam=$fparam\n\$hunk=$hunk\n" ~
                "\$hsize=$hsize\n" ~
                "\$number_of_32b_blocks=$number_of_32b_blocks\n" ~
                "\$part=$part\n" ~
                "\$repeater=$repeater\n" ~
                "\$hunk_appended=$hunk_appended\n"
            ).say if $!debug;

            my @length =
                ( $hunk ~~ m:g/../ ).map({ $_ if $_ ne '0x' });

            @out_param_array[$array_index] = sprintf("%064x", $var_data_offset);
            $var_data_offset += (($number_of_32b_blocks + 1) * 32) + 32;
            my $val = sprintf("%064x", @length.elems);
            ('***DEBUG > length: ' ~ $val).say if $!debug;
            @out_param_array[$var_data_offset_index + $second_pass_counter] = $val;
            $array_index++;
            $second_pass_counter++;

            my Int $hunk_position=0;
            while 1 {
                my Str $cur_str = substr($hunk_appended, $hunk_position, 64);
                if !$cur_str {
                    last;
                }
                @out_param_array[$var_data_offset_index + $second_pass_counter] = $cur_str;
                $second_pass_counter++;
                $hunk_position += 64;
            }
        }
    }
    if $fname eq 'constructor' {
        $raw = @out_param_array.join(q{});
    }
    else {
        # Get Contract Method Id
        my $contract_method_id = self.getContractMethodId(
            $fname ~ q{(} ~ ($param_types_list.chop if $param_types_list) ~ q{)}
        );
        $raw = $contract_method_id ~ @out_param_array.join(q{});
        ('***DEBUG > marshall array (method_id=' ~ ($contract_method_id || 'undefined')  ~ ')' ~ "\n" ~ @out_param_array.join("\n")).say if $!debug;
    }
    $raw;
}

method unmarshal(Str $fname, Str $raw_data) returns Hash {
    my %fabi = self.get_function_abi($fname);
    my $outputs = %fabi<outputs>;
    my %return_value;
    if $!return_raw_data {
        %return_value.push(raw_data => $raw_data);
    }

    my Int $current_out_param_position = 0;
    my Int $index_hash_key             = 0;
    ('***DEBUG > unmarshal outputs: ' ~ @$outputs.gist).say if $!debug;
    for @$outputs -> %out_param {
        my Str $cur_pname = %out_param<name> || $index_hash_key++;
        my Str $cur_ptype = %out_param<type>;

        if $cur_ptype eq 'bool' || $cur_ptype ~~ m:i/^ u?int / {
            my Str $chunk = '0x' ~ substr($raw_data, $current_out_param_position, 64);
            my $_udata   = self.unmarshal_int($chunk);
            %return_value.push($cur_pname => $_udata);
        }
        elsif $cur_ptype eq 'address' {
            my Str $chunk = '0x' ~ substr($raw_data, $current_out_param_position, 64);
            $chunk ~~ s:Perl5:g/00//;
            %return_value.push($cur_pname => $chunk);
        }
        elsif $cur_ptype eq 'bytes32' {
            my Str $chunk = '0x' ~ substr($raw_data, $current_out_param_position, 64);
            %return_value.push($cur_pname => $chunk);
        }
        elsif ($cur_ptype eq 'string' || $cur_ptype eq 'bytes') {
            my Int $size_offset = :16(substr($raw_data, $current_out_param_position, 64));
            my Int $data_size   = :16(substr($raw_data, $size_offset * 2, 64));
            my Str $data_chunk  = substr($raw_data, 64 + $size_offset*2, $data_size * 2);
            %return_value.push(
                $cur_pname => (
                    $cur_ptype eq 'string' ??
                        self.hex2string('0x' ~ $data_chunk) !!
                            self.hex2buf('0x' ~ $data_chunk)
                )
            );
        }
        else {
            die Net::Ethereum::X.new(:payload('unmarshal does not support type: ' ~ $cur_ptype));
        }
        $current_out_param_position += 64;
    }
    %return_value;
}

method sendTransaction(
    Str  :$account,
    Str  :$scid!,
    Str  :$fname!,
    Hash :$fparams!,
    UInt :$gas!,
    UInt :$gasprice,
    UInt :$value,
    UInt :$maxpriorityfeepergas,
) returns Str {
    my $feesdata = self.get_fee_data(:$maxpriorityfeepergas);
    my $raw      = self.marshal($fname, $fparams);

    my %par =
        from  => $account // self.get_account,
        to    => $scid,
        data  => $raw,
        gas   => sprintf("0x%s", $gas.base(16))
    ;

    %par<value> = sprintf("0x%s", $value.base(16)) if $value;

    if ($feesdata<maxFeePerGas> && $feesdata<maxPriorityFeePerGas> && !$gasprice) {
        %par<maxFeePerGas>         = sprintf("0x%s", ($feesdata<maxFeePerGas>).base(16));
        %par<maxPriorityFeePerGas> = sprintf("0x%s", ($feesdata<maxPriorityFeePerGas>).base(16));
    }
    else {
        %par<gasPrice> = sprintf("0x%s", $gasprice.base(16));
    }

    return self.eth_sendTransaction(%par);
}

method deploy_contract_estimate_gas(
    Str $src_account,
    Str $contract_binary,
    Hash $constructor_params
) returns Int {
    my %par = from => $src_account;

    if $constructor_params {
        my $raw = self.marshal( 'constructor', $constructor_params );
        %par.push: ( data => $contract_binary ~ $raw );
    }
    else {
        %par.push: ( data => $contract_binary );
    }

    return self.eth_estimateGas(%par);
}

method deploy_contract(
    Str $src_account,
    Str $contract_binary,
    Hash $constructor_params,
    Int $gas
) returns Str {
    my %par = from => $src_account;

    if $constructor_params {
        my $raw = self.marshal( 'constructor', $constructor_params );
        %par.push: ( data => $contract_binary ~ $raw);
    }
    else {
        %par.push: ( data => $contract_binary );
    }

    my $gas_recalc = $gas + floor($!gaspercent*$gas);

    %par.push: ( gas => '0x' ~ $gas_recalc.base(16) );

    self.eth_sendTransaction(%par);
}

method wait_for_transaction(
    :@hashes!,
    Int :$iters,
    Bool :$contract
) returns Array {
    my Str $new_contract_id;
    my @_hashes = @hashes;

    my @rc;

    for (0..($iters // $!tx_wait_iters)) {
        for @_hashes.kv -> $indx, $hash {
            next unless $hash ~~ m:i/^ 0x<xdigit>**64 $/;

            # sprintf("%d: %s", $indx, $hash).say;
            my %rc = self.eth_getTransactionByHash($hash);

            if %rc<blockNumber>:exists && %rc<blockNumber> {
                my %stat = self.eth_getTransactionReceipt($hash);

                if %stat<status>:exists {
                    if :16(%stat<status>) == 1  {
                        if $contract {
                            $new_contract_id = %stat<contractAddress>;
                            $!contract_id = $new_contract_id;
                        }

                        @rc.push(%stat);
                    }

                    @_hashes[$indx] = q{};
                }
                else {
                    ; # (@_hashes[$indx] ~ ': tx receipt is null at block ' ~ %rc<blockNumber>).say;
                }
            }
        }

        if @rc.elems == @hashes.elems {
            if $!show_progress {
                "\n".print;
            }

            last;
        } else {
            sleep($!tx_wait_sec);

            if $!show_progress {
                ( '.' ~ $_ ~ '(' ~ @rc.elems ~ '/' ~ @hashes.elems ~ ')' ).print;
            }
        }
    }

    # @rc.gist.say;

    return @rc;
}

method contract_method_call_estimate_gas(Str $fname, Hash $fparams, Str $src_account?) returns Int {
    my $acc = $src_account // self.get_account;
    my $raw = self.marshal($fname, $fparams);
    my %par =
        from => $acc,
        to   => ($!contract_id // die Net::Ethereum::X.new(:payload('$!contract_id is not set'))),
        data => $raw
    ;

    %par<from>:delete unless $acc;
    %par<data>:delete unless $raw && $raw ~~ /^ 0x<xdigit>+ $/ && $raw ne '0x0';

    self.eth_estimateGas(%par);
}

method contract_method_call(Str $fname, Hash $fparams, Str $src_account?) returns Hash {
    my $acc = $src_account // self.get_account;
    my $raw = self.marshal($fname, $fparams);
    my %par = from => $acc, to => ($!contract_id // die Net::Ethereum::X.new(:payload('$!contract_id is not set'))), data => $raw;

    %par<from>:delete unless $acc;

    if $!debug {
        ( 'Function name: '       ~ $fname ).say;
        ( 'Function params: '     ~ %par.gist ).say;
        ( 'Function raw params: ' ~ $raw ).say;
    }

    my $rc = self.eth_call(%par);
    if $!debug {
        ('eth_call return data: ' ~ $rc).say;
    }
    $raw = substr($rc, 2);
    self.unmarshal($fname, $raw);
}

method compile_and_deploy_contract(
    Str  :$contract_path!,
    Str  :$compile_output_path!,
    Hash :$constructor_params,
    Str  :$account,
    Str  :$password,
    Bool :$skipcompile = False,
    Bool :$skipunlock  = False
) returns Hash {
    my Str $acc = $account // self.get_account;
    my Str $pwd = $password // $!unlockpwd;

    my Str $contract_name = ($contract_path.IO.basename.IO.extension: '').Str;

    if !$skipcompile {
        my Str $cmd = $!solidity ~ ' --evm-version ' ~ $!evmver ~ ' --bin --abi ' ~ $contract_path ~ ' -o ' ~ $compile_output_path  ~ ' --overwrite';

        ('***DEBUG > solidity path: ' ~ $!solidity ~ ', command: ' ~ $cmd ~ ', contract name: ' ~ $contract_name).say if $!debug;

        if !$compile_output_path.IO.e || !$!solidity.IO.e || !shell($cmd) {
            die Net::Ethereum::X.new(:payload('failed to compile ' ~ $contract_name ~ ' with command: ' ~ $cmd));
        }
    }

    my Str $bin_path = sprintf("%s/%s.bin", $compile_output_path, $contract_name);
    my Str $abi_path = sprintf("%s/%s.abi", $compile_output_path, $contract_name);

    die Net::Ethereum::X.new(:payload('could not deploy smart contract: no abi and/or bin files')) unless
        $bin_path.IO.e && $bin_path.IO.f && $abi_path.IO.e && $abi_path.IO.f;

    my Str $bin = '0x' ~ $bin_path.IO.slurp;
    $!abi       = $abi_path.IO.slurp;

    self.personal_unlockAccount(:account($acc), :password($pwd)) unless $skipunlock;

    my $contract_used_gas     = self.deploy_contract_estimate_gas($acc, $bin, $constructor_params);
    my $gas_price             = self.eth_gasPrice;
    my $contract_deploy_price = $contract_used_gas * $gas_price;

    ('***DEBUG > contract gas estimated: ' ~ $contract_used_gas ~ ' wei (0x' ~ $contract_used_gas.base(16) ~ q{)} ~ "\n").say if $!debug;

    my $contract_deploy_tr = self.deploy_contract($acc, $bin, $constructor_params, $contract_used_gas);
    my %contract_status    = self.wait_for_transaction(:hashes(@$contract_deploy_tr), :contract(True)).head;

    die Net::Ethereum::X.new(:payload(sprintf("no transaction %s mined in %d iterations per %d sec", $contract_deploy_tr, $!tx_wait_iters, $!tx_wait_sec))) unless
        %contract_status && %contract_status.keys;

    my Str $contract_code;

    for ^$!tx_wait_iters -> $iter {
        $contract_code = self.eth_getCode(%contract_status<contractAddress>, 'latest');

        last if $contract_code ~~ m:i/^ 0x<xdigit> ** 16..* $/;

        sleep($!tx_wait_sec);
    }

    die Net::Ethereum::X.new(:payload(sprintf("no contract code for %s (%s)\n%s", $contract_deploy_tr, $contract_code, to-json(%contract_status)))) if
        $contract_code eq '0x';

    if $!cleanup and !$skipcompile {
        for $compile_output_path.IO.dir( test => /:i <{$contract_name}> '.' <[a..z]>+ $/ ) -> $file {
            if !$file.unlink {
                sprintf("***DEBUG > could not unlink at cleanup with %s", $file.basename).say if $!debug;
            }
        }
    }

    %contract_status;
}

method retrieve_contract(Str $hash) returns Str {
   my $rq = %( jsonrpc => "2.0", method => "eth_getTransactionReceipt", params => [ ~($hash) ]);
   self.node_request($rq)<result><contractAddress> || "0x" ~ q{0} x 40;
}

# https://github.com/ethers-io/ethers.js/blob/f97b92bbb1bde22fcc44100af78d7f31602863ab/packages/abstract-provider/src.ts/index.ts#L235
method get_fee_data(
    UInt :$maxpriorityfeepergas = 0,
    UInt :$maxfeepergas = 0,
    UInt :$basefeeperblobgas = 0,
    UInt :$maxfeeperblobgas = 0,
) returns Hash {
    return {
        maxPriorityFeePerGas => $maxpriorityfeepergas,
        maxFeePerGas         => $maxfeepergas
    } if $maxpriorityfeepergas && $maxfeepergas;

    my $block    = self.eth_getBlockByNumber("latest");
    my $gasprice = self.eth_gasPrice;

    my $lastBaseFeePerGas;
    my $maxFeePerGas;
    my $maxPriorityFeePerGas;
    my $baseFeePerBlobGas;
    my $maxFeePerBlobGas;

    if $block {
        if $block<baseFeePerGas> {
            $lastBaseFeePerGas    = $block<baseFeePerGas>.Int;
            $maxPriorityFeePerGas = $maxpriorityfeepergas || self.eth_maxPriorityFeePerGas;
            $maxFeePerGas         = $maxfeepergas || (($lastBaseFeePerGas * 2) + $maxPriorityFeePerGas);
        }

        if ($block<excessBlobGas>:exists) && ($block<blobGasUsed>:exists) {
            $baseFeePerBlobGas = $basefeeperblobgas || self.get_base_fee_per_blob_gas(:$block);
            $maxFeePerBlobGas  = $maxfeeperblobgas || ceiling($baseFeePerBlobGas * 1.1);
        }
    }

    my $feedata = {
        gasPrice             => $gasprice,
        lastBaseFeePerGas    => $lastBaseFeePerGas,
        maxPriorityFeePerGas => $maxPriorityFeePerGas,
        maxFeePerGas         => $maxFeePerGas,
        baseFeePerBlobGas    => $baseFeePerBlobGas,
        maxFeePerBlobGas     => $maxFeePerBlobGas,
    }

    sprintf("***DEBUG > fee data %s", $feedata.gist).say if $!debug;

    return $feedata;
}

method get_base_fee_per_blob_gas(Hash :$block!) returns Int {
    die Net::Ethereum::X::InvalidBlock.new(:payload('block struct has no blob gas related members')) unless
        $block.keys && ($block<excessBlobGas>:exists) && ($block<blobGasUsed>:exists);

    my Int $excess_blob_gas = 0;
    my Int $block_blob_gas  = $block<excessBlobGas>.Int + $block<blobGasUsed>.Int;

    if $block_blob_gas >= TARGET_BLOB_GAS_PER_BLOCK {
        $excess_blob_gas = $block_blob_gas - TARGET_BLOB_GAS_PER_BLOCK;
    }

    return $!ethutils.fake_exponential(
        :factor(MIN_BASE_FEE_PER_BLOB_GAS),
        :numerator($excess_blob_gas),
        :denominator(BLOB_BASE_FEE_UPDATE_FRACTION)
    );
}
