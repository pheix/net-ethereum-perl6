unit class Net::Ethereum::Utils;

method fake_exponential(Int :$factor!, Int :$numerator!, Int :$denominator!) returns Int {
    my $i = 1;
    my $output = 0;
    my $numerator_accum = $factor * $denominator;

    while ($numerator_accum > 0) {
        $output         += $numerator_accum;
        $numerator_accum = floor(($numerator_accum * $numerator) / ($denominator * $i));

        $i += 1;
    }

    return floor($output / $denominator);
}
