#!/bin/sh

# If use with Parity node check that parameter --jsonrpc-hosts is set to all value
# example: parity --jsonrpc-hosts=all ...

PORTI=8501
PORTO=8540

if [ ! -z $1 ] && [ ! -z $2 ]; then
    PORTI=$1
    PORTO=$2
    echo "Use user-defined ports: forward from ${PORTI} to ${PORTO}"
else
    echo "Use default ports: forward from ${PORTI} to ${PORTO}"
fi

socat TCP-LISTEN:${PORTI},reuseaddr,fork TCP:127.0.0.1:${PORTO}

